sap.ui.define([
	"com/firehana/webapp/controller/BaseController",
	"sap/ui/model/resource/ResourceModel",
		"sap/ui/model/json/JSONModel"
], function(BaseController, ResourceModel, JSONModel) {
	"use strict";
	return BaseController.extend("com.firehana.webapp.controller.MainPage", {
		onInit: function() {
			var i18nModel = new ResourceModel({
				bundleName: "com.firehana.webapp.i18n.i18n"
			});
			this.setModel(i18nModel, "i18n");
			
			var viewModel = new JSONModel({
				admin: false
			});
			this.setModel(viewModel, "viewModel");
			
						
			var self = this;
			this.ajax("/api/admin/ping", "GET", {}, 
				function() {
					self.getModel("viewModel").setProperty("/admin", true);
				}, undefined, true, true);
		},
		onOpenCreateTriggerDialog: function() {
			var oView = this.getView();
			var oDialog = oView.byId("createTriggerDialog");
			if (!oDialog) {
				oDialog = sap.ui.xmlfragment(oView.getId(), "com.firehana.webapp.view.component.CreateTriggerDialog", this);
				oView.addDependent(oDialog);
			}

			oDialog.open();
		},
		onCloseCreateTriggerDialog: function() {
			this.getView().byId("createTriggerDialog").close();
		},
		onOpenAdministration: function() {
			this.getRouter().navTo("admin");
		},
		onCreateTrigger: function() {
			var name = this.getView().byId("inputTriggerName").getValue();

			if (name && name.length > 5) {
				var onlyCharacters = true;
				for (var i=0; i<name.length; i++) {
					if (!name.charAt(i).match(/[a-z]/i)) {
						onlyCharacters = false;
						break;
					}
				}
				if (onlyCharacters) {
					var self = this;
					this.ajax("/api/trigger/trigger", "POST", {
						name: name
					},
					function(result) {
						var eventBus = sap.ui.getCore().getEventBus();
						eventBus.publish("UPDATE", "NEW_TRIGGER", result.trigger);

						self.getView().byId("createTriggerDialog").close();
						self.getRouter().navTo("trigger", {
							triggerId: result.trigger.mkey
						});
					}, undefined, false);	
				} else {
					this.showErrorDialog(this.getModel("i18n").getResourceBundle().getText("main_createTrigger_errorCharacters"));
				}
			} else {
				this.showErrorDialog(this.getModel("i18n").getResourceBundle().getText("main_createTrigger_errorTooShort"));
			}
		}
	});
});