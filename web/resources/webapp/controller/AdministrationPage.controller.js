sap.ui.define([
	"com/firehana/webapp/controller/BaseController",
	"sap/ui/model/json/JSONModel",
	"sap/m/MessageToast",
	"sap/m/Button",
	"sap/m/Dialog",
	"sap/m/Text",
	"sap/ui/model/resource/ResourceModel"
], function(BaseController, JSONModel, MessageToast, Button, Dialog, Text, ResourceModel) {
	"use strict";
	return BaseController.extend("com.firehana.webapp.controller.AdministrationPage", {
		onInit: function() {
			var i18nModel = new ResourceModel({
				bundleName: "com.firehana.webapp.i18n.i18n"
			});
			this.setModel(i18nModel, "i18n");
	        
		}
	});
});