sap.ui.define([
	"com/firehana/webapp/controller/BaseController",
	"sap/ui/model/json/JSONModel",
	"sap/m/MessageToast",
	"sap/m/Button",
	"sap/m/Dialog",
	"sap/m/Text",
	"sap/ui/model/resource/ResourceModel"
], function(BaseController, JSONModel, MessageToast, Button, Dialog, Text, ResourceModel) {
	"use strict";
	return BaseController.extend("com.firehana.webapp.controller.AdminLogs", {
		onInit: function() {
			var i18nModel = new ResourceModel({
				bundleName: "com.firehana.webapp.i18n.i18n"
			});
			this.setModel(i18nModel, "i18n");
	        			var viewModel = new JSONModel({
				logs: {
					logs: [],
					currentPage: 1,
					maxPages: 1,
					loading: true,
					messageFilter: "",
					levelFilter: "",
					userFilter: ""
				}
			});

			this.setModel(viewModel, "viewModel");

			this.onReloadLogs();
		},
		onLoadNextLogs: function() {
			this.getModel("viewModel").setProperty("/logs/currentPage", parseInt(this.getModel("viewModel").getProperty("/logs/currentPage")) + 1);
			this.onReloadLogs();	
		},
		onLoadPreviousLogs: function() {
			this.getModel("viewModel").setProperty("/logs/currentPage", parseInt(this.getModel("viewModel").getProperty("/logs/currentPage")) - 1);
			this.onReloadLogs();	
		},
		onReloadLogs: function() {
			this.getModel("viewModel").setProperty("/logs/loading", true);
			var self = this;
			this.ajax("/api/admin/logs", "POST", {
				page: self.getModel("viewModel").getProperty("/logs/currentPage"),
				messageFilter: self.getModel("viewModel").getProperty("/logs/messageFilter"),
				levelFilter: self.getModel("viewModel").getProperty("/logs/levelFilter"),
				userFilter: self.getModel("viewModel").getProperty("/logs/userFilter")
			}, 
				function(result) {
					self.getModel("viewModel").setProperty("/logs/loading", false);
					self.getModel("viewModel").setProperty("/logs/currentPage", result.currentPage);
					self.getModel("viewModel").setProperty("/logs/maxPages", result.maxPages);
					self.getModel("viewModel").setProperty("/logs/logs", result.logs);
				});	
		}
	});
});