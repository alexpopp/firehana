/*global history */
sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/routing/History",
	"sap/m/Button",
	"sap/m/Dialog",
	"sap/m/Text"
], function(Controller, History, Button, Dialog, Text) {
	"use strict";

	return Controller.extend("com.firehana.webapp.controller.BaseController", {
		

		/**
		 * Convenience method for accessing the router in every controller of the application.
		 * @public
		 * @returns {sap.ui.core.routing.Router} the router for this component
		 */
		getRouter: function() {
			return this.getOwnerComponent().getRouter();
		},

		/**
		 * Convenience method for getting the view model by name in every controller of the application.
		 * @public
		 * @param {string} sName the model name
		 * @returns {sap.ui.model.Model} the model instance
		 */
		getModel: function(sName) {
			return this.getView().getModel(sName);
		},

		/**
		 * Convenience method for setting the view model in every controller of the application.
		 * @public
		 * @param {sap.ui.model.Model} oModel the model instance
		 * @param {string} sName the model name
		 * @returns {sap.ui.mvc.View} the view instance
		 */
		setModel: function(oModel, sName) {
			return this.getView().setModel(oModel, sName);
		},

		/**
		 * Convenience method for getting the resource bundle.
		 * @public
		 * @returns {sap.ui.model.resource.ResourceModel} the resourceModel of the component

		getResourceBundle : function () {
			return this.getOwnerComponent().getModel("i18n").getResourceBundle();
		},
		*/
		/**
		 * Event handler for navigating back.
		 * It there is a history entry we go one step back in the browser history
		 * If not, it will replace the current entry of the browser history with the master route.
		 * @public
		 */
		onNavBack: function() {
			var sPreviousHash = History.getInstance().getPreviousHash();

			if (sPreviousHash !== undefined) {
				history.go(-1);
			} else {
				this.getRouter().navTo("main", {}, true);
			}
		},

		ajax: function(url, method, data, successCallback, errorCallback, hideBusyIndicator, hideErrorDialog) {
			if (!hideBusyIndicator) {
				sap.ui.core.BusyIndicator.show(100);
			}

			var self = this;
			jQuery.ajax({
				url: url,
				method: method,
				dataType: "json",
				data: data,
				async: true
			}).done(function(result) {
				if (successCallback) {
					successCallback(result);
				}
			}).fail(function(error) {
				if (!hideErrorDialog) {
					var errorMessage = JSON.stringify(error.responseText).replace(/{/g, "(").replace(/}/g, ")");
					self.showErrorDialog(errorMessage);	
				}
				if (errorCallback) {
					errorCallback(error);
				}
			}).complete(function() {
				if (!hideBusyIndicator) {
					sap.ui.core.BusyIndicator.hide();
				}
			});
		},
		showConfirmDialog: function(message, title, okCallback) {
			if (!title) {
				title = "Warning";
			}
			var self = this;
			var dialog = new Dialog({
				title: title,
				type: "Message",
				state: "Warning",
				content: new Text({ text: message }),
				beginButton: new Button({
					text: 'Ok',
					press: function () {
						dialog.close();
						okCallback();
					}
				}),
				endButton: new Button({
					text: self.getModel("i18n").getResourceBundle().getText("dialog_cancel"),
					press: function () {
						dialog.close();
					}
				}),
				afterClose: function() {
					dialog.destroy();
				}
			});

			dialog.open();
		},
		showErrorDialog: function(message, title) {
			if (!title) {
				title = "Error";
			}
			var dialog = new Dialog({
					title: title,
					type: 'Message',
					state: 'Error',
					content: new Text({
						text: message
					}),
					beginButton: new Button({
						text: 'OK',
						press: function() {
							dialog.close();
						}
					}),
					afterClose: function() {
						dialog.destroy();
					}
				});

				dialog.open();	
		}

	});

});