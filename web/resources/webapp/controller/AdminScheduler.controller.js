sap.ui.define([
	"com/firehana/webapp/controller/BaseController",
	"sap/ui/model/json/JSONModel",
	"sap/m/MessageToast",
	"sap/m/Button",
	"sap/m/Dialog",
	"sap/m/Text",
	"sap/ui/model/resource/ResourceModel"
], function(BaseController, JSONModel, MessageToast, Button, Dialog, Text, ResourceModel) {
	"use strict";
	return BaseController.extend("com.firehana.webapp.controller.AdminScheduler", {
		onInit: function() {
			var i18nModel = new ResourceModel({
				bundleName: "com.firehana.webapp.i18n.i18n"
			});
			this.setModel(i18nModel, "i18n");
	        
	        var self = this;
			var viewModel = new JSONModel({
				eventScheduler: {},
				notificationScheduler: {},
				availableCrons: [{
					key: "ONE_SECOND",
					name: self.getModel("i18n").getResourceBundle().getText("scheduler_oneSecond"),
					index: 0
				}, {
					key: "FIVE_SECONDS",
					name: self.getModel("i18n").getResourceBundle().getText("scheduler_fiveSeconds"),
					index: 1
				}, {
					key: "ONE_MINUTE",
					name: self.getModel("i18n").getResourceBundle().getText("scheduler_oneMinute"),
					index: 2
				}, {
					key: "TWO_MINUTES",
					name: self.getModel("i18n").getResourceBundle().getText("scheduler_twoMinutes"),
					index: 3
				}, {
					key: "ONE_HOUR",
					name: self.getModel("i18n").getResourceBundle().getText("scheduler_oneHour"),
					index: 4
				}]
			});
			this.setModel(viewModel, "viewModel");

						
			this.ajax("/api/admin/eventscheduler", "GET", {}, 
				function(result) {
					self.updateEventSchedulerUI(result.mkey, result.cron);
				});
				
			this.ajax("/api/admin/notificationscheduler", "GET", {}, 
				function(result) {
					if (result.mkey) {
						self.getModel("viewModel").setProperty("/notificationScheduler/active", true);
					} else {
						self.getModel("viewModel").setProperty("/notificationScheduler/active", false);
					}
				});
		},
		updateEventSchedulerUI: function(active, cron) {
			if (active) {
				var translation = "";
				for (var i=0; i<this.getModel("viewModel").getProperty("/availableCrons").length; i++) {
					if (this.getModel("viewModel").getProperty("/availableCrons")[i].key === cron) {
						translation = this.getModel("viewModel").getProperty("/availableCrons")[i].name;
						break;
					}
				}
				this.getModel("viewModel").setProperty("/eventScheduler/active", true);
				this.getModel("viewModel").setProperty("/eventScheduler/text", this.getModel("i18n").getResourceBundle().getText("admin_schedulerActivated") + " (" + translation + ")");	
			} else {
				this.getModel("viewModel").setProperty("/eventScheduler/active", false);
				this.getModel("viewModel").setProperty("/eventScheduler/text", this.getModel("i18n").getResourceBundle().getText("admin_schedulerDeactivated"));	
			}
		},
		onOpenCreateSchedulerDialog: function() {
			var oView = this.getView();
			var oDialog = oView.byId("createSchedulerDialog");
			if (!oDialog) {
				oDialog = sap.ui.xmlfragment(oView.getId(), "com.firehana.webapp.view.component.CreateSchedulerDialog", this);
				oView.addDependent(oDialog);
			}
			
			oDialog.open();	
		},
		onCloseCreateSchedulerDialog: function() {
			this.getView().byId("createSchedulerDialog").close();		
		},
		onCreateEventScheduler: function() {
			var oView = this.getView();
			var oDialog = oView.byId("createSchedulerDialog");
			var cronKey = this.getView().byId("inputCron").getSelectedKey();
			oDialog.close();
			
			var self = this;
			this.ajax("/api/admin/eventScheduler", "POST", {
					action: "start",
					cronKey: cronKey,
					url: "https://"+window.location.hostname+":"+window.location.port
				}, 
				function() {
					self.updateEventSchedulerUI(true, cronKey);
					MessageToast.show(self.getModel("i18n").getResourceBundle().getText("admin_schedulerActivated"));
				});
		},
		onCreateNotificationScheduler: function() {
			var self = this;
			this.ajax("/api/admin/notificationscheduler", "POST", {
					action: "start",
					url: "https://"+window.location.hostname+":"+window.location.port
				}, 
				function() {
					self.getModel("viewModel").setProperty("/notificationScheduler/active", true);
					MessageToast.show(self.getModel("i18n").getResourceBundle().getText("admin_schedulerActivated"));
				});
		},
		onStopEventScheduler: function() {
			var self = this;
			var dialog = new Dialog({
				title: self.getModel("i18n").getResourceBundle().getText("scheduler_confirmTitle"),
				type: 'Message',
				content: new Text({
					text: self.getModel("i18n").getResourceBundle().getText("scheduler_confirmText")
				}),
				beginButton: new Button({
					text: self.getModel("i18n").getResourceBundle().getText("scheduler_confirmYes"),
					press: function() {
						self.ajax("/api/admin/eventScheduler", "POST", {
							action: "stop"
						}, 
						function() {
							self.updateEventSchedulerUI(false, undefined);
							MessageToast.show(self.getModel("i18n").getResourceBundle().getText("admin_schedulerDeactivated"));
						});
						dialog.close();
					}
				}),
				endButton: new Button({
					text: self.getModel("i18n").getResourceBundle().getText("dialog_cancel"),
					press: function() {
						dialog.close();
					}
				}),
				afterClose: function() {
					dialog.destroy();
				}
			});

			dialog.open();
		},
		onStopNotificationScheduler: function() {
			var self = this;
			var dialog = new Dialog({
				title: self.getModel("i18n").getResourceBundle().getText("scheduler_confirmTitle"),
				type: 'Message',
				content: new Text({
					text: self.getModel("i18n").getResourceBundle().getText("scheduler_confirmText")
				}),
				beginButton: new Button({
					text: self.getModel("i18n").getResourceBundle().getText("scheduler_confirmYes"),
					press: function() {
						self.ajax("/api/admin/notificationscheduler", "POST", {
							action: "stop"
						}, 
						function() {
							self.getModel("viewModel").setProperty("/notificationScheduler/active", false);
							MessageToast.show(self.getModel("i18n").getResourceBundle().getText("admin_schedulerDeactivated"));
						});
						dialog.close();
					}
				}),
				endButton: new Button({
					text: self.getModel("i18n").getResourceBundle().getText("dialog_cancel"),
					press: function() {
						dialog.close();
					}
				}),
				afterClose: function() {
					dialog.destroy();
				}
			});

			dialog.open();
		}
	});
});