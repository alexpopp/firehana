sap.ui.define([
	"com/firehana/webapp/controller/BaseController",
	"sap/ui/model/json/JSONModel",
	"sap/m/MessageToast",
	"sap/m/Button",
	"sap/m/Dialog",
	"sap/m/Text",
	"sap/ui/model/resource/ResourceModel"
], function(BaseController, JSONModel, MessageToast, Button, Dialog, Text, ResourceModel) {
	"use strict";
	return BaseController.extend("com.firehana.webapp.controller.AdminNotifications", {
		onInit: function() {
			var i18nModel = new ResourceModel({
				bundleName: "com.firehana.webapp.i18n.i18n"
			});
			this.setModel(i18nModel, "i18n");
	        
			var configModel = new JSONModel({});
			var testModel = new JSONModel({});
			this.setModel(configModel, "configModel");
			this.setModel(testModel, "testModel");

	        var self = this;
			this.ajax("/api/admin/config/mail", "GET", {}, 
				function(result) {
					self.getModel("configModel").setProperty("/mail", result);
				});
			this.ajax("/api/admin/config/slack", "GET", {}, 
				function(result) {
					self.getModel("configModel").setProperty("/slack", result);
				});
			this.ajax("/api/admin/config/twilio", "GET", {}, 
				function(result) {
					self.getModel("configModel").setProperty("/twilio", result);
				});
			this.ajax("/api/admin/config/audit", "GET", {}, 
				function(result) {
					self.getModel("configModel").setProperty("/audit", result);
				});
			this.ajax("/api/admin/config/android", "GET", {}, 
				function(result) {
					self.getModel("configModel").setProperty("/android", result);
				});
			this.ajax("/api/admin/config/rest", "GET", {}, 
				function(result) {
					self.getModel("configModel").setProperty("/rest", result);
				});
			this.ajax("/api/admin/config/stride", "GET", {}, 
				function(result) {
					self.getModel("configModel").setProperty("/stride", result);
				});
			this.ajax("/api/admin/config/hangout", "GET", {}, 
				function(result) {
					self.getModel("configModel").setProperty("/hangout", result);
				});
		},
		onCloseTestConnectionDialog: function() {
			this.getView().byId("sendTestNotification").close();	
		},
		onOpenTestConnectionDialog: function(evt) {
			var medium = evt.getSource().data("medium");
			var configModel = this.getModel("configModel");
			var configuration = [];
			switch (medium) {
				case "KEY_CONFIG_MAIL_ACTIVE":
					configuration.push({key: "KEY_CONFIG_MAIL_HOST", value: configModel.getProperty("/mail/KEY_CONFIG_MAIL_HOST"), label: this.getModel("i18n").getResourceBundle().getText("admin_email_host")});
					configuration.push({key: "KEY_CONFIG_MAIL_PORT", value: configModel.getProperty("/mail/KEY_CONFIG_MAIL_PORT"), label: this.getModel("i18n").getResourceBundle().getText("admin_email_port")});
					configuration.push({key: "KEY_CONFIG_MAIL_USERNAME", value: configModel.getProperty("/mail/KEY_CONFIG_MAIL_USERNAME"), label: this.getModel("i18n").getResourceBundle().getText("admin_email_user")});
					configuration.push({key: "KEY_CONFIG_MAIL_PASSWORD", value: configModel.getProperty("/mail/KEY_CONFIG_MAIL_PASSWORD"), label: this.getModel("i18n").getResourceBundle().getText("admin_email_password")});
					configuration.push({key: "KEY_CONFIG_MAIL_FROM", value: configModel.getProperty("/mail/KEY_CONFIG_MAIL_FROM"), label: this.getModel("i18n").getResourceBundle().getText("admin_email_from")});
					configuration.push({key: "KEY_CONFIG_MAIL_TLS", value: configModel.getProperty("/mail/KEY_CONFIG_MAIL_TLS"), label: this.getModel("i18n").getResourceBundle().getText("admin_email_tls")});
					configuration.push({key: "KEY_CONFIG_MAIL_REJECTUNAUTH", value: configModel.getProperty("/mail/KEY_CONFIG_MAIL_REJECTUNAUTH"), label: this.getModel("i18n").getResourceBundle().getText("admin_email_rejectunauth")});
					configuration.push({key: "KEY_CONFIG_MAIL_ACTIVE", value: configModel.getProperty("/mail/KEY_CONFIG_MAIL_ACTIVE"), label: this.getModel("i18n").getResourceBundle().getText("admin_email_active")});
					break;
				case "KEY_CONFIG_SLACK_ACTIVE":
					configuration.push({key: "KEY_CONFIG_SLACK_HOOK", value: configModel.getProperty("/slack/KEY_CONFIG_SLACK_HOOK"), label: this.getModel("i18n").getResourceBundle().getText("admin_slack_hook")});
					break;
				case "KEY_CONFIG_STRIDE_ACTIVE":
					break;
				case "KEY_CONFIG_TWILIO_ACTIVE":
					configuration.push({key: "KEY_CONFIG_TWILIO_SID", value: configModel.getProperty("/twilio/KEY_CONFIG_TWILIO_SID"), label: this.getModel("i18n").getResourceBundle().getText("admin_twilio_sid")});
					configuration.push({key: "KEY_CONFIG_TWILIO_AUTHTOKEN", value: configModel.getProperty("/twilio/KEY_CONFIG_TWILIO_AUTHTOKEN"), label: this.getModel("i18n").getResourceBundle().getText("admin_twilio_authtoken")});
					break;
				case "KEY_CONFIG_ANDROID_ACTIVE":
					var fb_private_key = configModel.getProperty("/android/KEY_CONFIG_ANDROID_PRIVATEKEY");
					configuration.push({key: "KEY_CONFIG_ANDROID_PROJECTID", value: configModel.getProperty("/android/KEY_CONFIG_ANDROID_PROJECTID"), label: this.getModel("i18n").getResourceBundle().getText("admin_android_projectid")});
					configuration.push({key: "KEY_CONFIG_ANDROID_PRIVATEKEYID", value: configModel.getProperty("/android/KEY_CONFIG_ANDROID_PRIVATEKEYID"), label: this.getModel("i18n").getResourceBundle().getText("admin_android_privatekeyid")});
					configuration.push({key: "KEY_CONFIG_ANDROID_PRIVATEKEY", value: fb_private_key, label: this.getModel("i18n").getResourceBundle().getText("admin_android_privatekey")});
					configuration.push({key: "KEY_CONFIG_ANDROID_EMAIL", value: configModel.getProperty("/android/KEY_CONFIG_ANDROID_EMAIL"), label: this.getModel("i18n").getResourceBundle().getText("admin_android_email")});
					configuration.push({key: "KEY_CONFIG_ANDROID_CLIENTID", value: configModel.getProperty("/android/KEY_CONFIG_ANDROID_CLIENTID"), label: this.getModel("i18n").getResourceBundle().getText("admin_android_clientid")});
					configuration.push({key: "KEY_CONFIG_ANDROID_CERTURL", value: configModel.getProperty("/android/KEY_CONFIG_ANDROID_CERTURL"), label: this.getModel("i18n").getResourceBundle().getText("admin_android_certurl")});
					break;
				case "KEY_CONFIG_AUDIT_ACTIVE":
					break;
				case "KEY_CONFIG_REST_ACTIVE":
					break;
				case "KEY_CONFIG_HANGOUT_ACTIVE":
					break;
			}
			
			this.getModel("testModel").setProperty("/configuration", configuration);
			this.getModel("testModel").setProperty("/medium", medium);
			this.getModel("testModel").setProperty("/text", "Example text");
			this.getModel("testModel").setProperty("/detail1", "");
			this.getModel("testModel").setProperty("/detail2", "");
			
			var oView = this.getView();
			var oDialog = oView.byId("sendTestNotification");
			if (!oDialog) {
				oDialog = sap.ui.xmlfragment(oView.getId(), "com.firehana.webapp.view.component.SendTestNotification", this);
				oView.addDependent(oDialog);
			}

			oDialog.open();
		},
		sendTestConnection: function() {
			var configuration = this.getModel("testModel").getProperty("/configuration");
			var medium = this.getModel("testModel").getProperty("/medium");
			var text = this.getModel("testModel").getProperty("/text");
			var detail1 = this.getModel("testModel").getProperty("/detail1");
			var detail2 = this.getModel("testModel").getProperty("/detail2");
			
			var self = this;
			this.ajax("/api/admin/config/test", "POST", {
					medium: medium,
					configuration: configuration,
					text: text,
					detail1: detail1,
					detail2: detail2
				}, function() {
					MessageToast.show(self.getModel("i18n").getResourceBundle().getText("admin_test_sent"));
				});
		},
		onUpdateMailConfig: function() {
			var mailObject = {};
			var configModel = this.getModel("configModel");
			if (configModel.getProperty("/mail/KEY_CONFIG_MAIL_HOST")) {
				mailObject.KEY_CONFIG_MAIL_HOST = configModel.getProperty("/mail/KEY_CONFIG_MAIL_HOST");
			}
			if (configModel.getProperty("/mail/KEY_CONFIG_MAIL_PORT")) {
				mailObject.KEY_CONFIG_MAIL_PORT = configModel.getProperty("/mail/KEY_CONFIG_MAIL_PORT");
			}
			if (configModel.getProperty("/mail/KEY_CONFIG_MAIL_USERNAME")) {
				mailObject.KEY_CONFIG_MAIL_USERNAME = configModel.getProperty("/mail/KEY_CONFIG_MAIL_USERNAME");
			}
			if (configModel.getProperty("/mail/KEY_CONFIG_MAIL_PASSWORD")) {
				mailObject.KEY_CONFIG_MAIL_PASSWORD = configModel.getProperty("/mail/KEY_CONFIG_MAIL_PASSWORD");
			}
			if (configModel.getProperty("/mail/KEY_CONFIG_MAIL_FROM")) {
				mailObject.KEY_CONFIG_MAIL_FROM = configModel.getProperty("/mail/KEY_CONFIG_MAIL_FROM");
			}
			mailObject.KEY_CONFIG_MAIL_TLS = configModel.getProperty("/mail/KEY_CONFIG_MAIL_TLS");
			mailObject.KEY_CONFIG_MAIL_REJECTUNAUTH = configModel.getProperty("/mail/KEY_CONFIG_MAIL_REJECTUNAUTH");
			mailObject.KEY_CONFIG_MAIL_ACTIVE = configModel.getProperty("/mail/KEY_CONFIG_MAIL_ACTIVE");
			
			var self = this;
			this.ajax("/api/admin/config/mail", "POST", mailObject, 
						function() {
							MessageToast.show(self.getModel("i18n").getResourceBundle().getText("admin_notification_updated"));
						});
		},
		onUpdateSlackConfig: function() {
			var slackObject = {};
			var configModel = this.getModel("configModel");
			if (configModel.getProperty("/slack/KEY_CONFIG_SLACK_HOOK")) {
				slackObject.KEY_CONFIG_SLACK_HOOK = configModel.getProperty("/slack/KEY_CONFIG_SLACK_HOOK");
			}
			slackObject.KEY_CONFIG_SLACK_ACTIVE = configModel.getProperty("/slack/KEY_CONFIG_SLACK_ACTIVE");
			
			var self = this;
			this.ajax("/api/admin/config/slack", "POST", slackObject, 
					function() {
						MessageToast.show(self.getModel("i18n").getResourceBundle().getText("admin_notification_updated"));
					});
		},
		onUpdateStrideConfig: function() {
			var strideObject = {};
			var configModel = this.getModel("configModel");
			strideObject.KEY_CONFIG_STRIDE_ACTIVE = configModel.getProperty("/stride/KEY_CONFIG_STRIDE_ACTIVE");
			
			var self = this;
			this.ajax("/api/admin/config/stride", "POST", strideObject, 
					function() {
						MessageToast.show(self.getModel("i18n").getResourceBundle().getText("admin_notification_updated"));
					});	
		},
		onUpdateTwilioConfig: function() {
			var twilioObject = {};
			var configModel = this.getModel("configModel");
			if (configModel.getProperty("/twilio/KEY_CONFIG_TWILIO_SID")) {
				twilioObject.KEY_CONFIG_TWILIO_SID = configModel.getProperty("/twilio/KEY_CONFIG_TWILIO_SID");
			}
			if (configModel.getProperty("/twilio/KEY_CONFIG_TWILIO_AUTHTOKEN")) {
				twilioObject.KEY_CONFIG_TWILIO_AUTHTOKEN = configModel.getProperty("/twilio/KEY_CONFIG_TWILIO_AUTHTOKEN");
			}
			twilioObject.KEY_CONFIG_TWILIO_ACTIVE = configModel.getProperty("/twilio/KEY_CONFIG_TWILIO_ACTIVE");
			
			var self = this;
			this.ajax("/api/admin/config/twilio", "POST", twilioObject, 
					function() {
						MessageToast.show(self.getModel("i18n").getResourceBundle().getText("admin_notification_updated"));
					});	
		},
		onUpdateAuditConfig: function() {
			var auditObject = {};
			var configModel = this.getModel("configModel");
			auditObject.KEY_CONFIG_AUDIT_ACTIVE = configModel.getProperty("/audit/KEY_CONFIG_AUDIT_ACTIVE");
			
			var self = this;
			this.ajax("/api/admin/config/audit", "POST", auditObject, 
					function() {
						MessageToast.show(self.getModel("i18n").getResourceBundle().getText("admin_notification_updated"));
					});	
		},
		onUpdateRESTConfig: function() {
			var RESTObject = {};
			var configModel = this.getModel("configModel");
			RESTObject.KEY_CONFIG_REST_ACTIVE = configModel.getProperty("/rest/KEY_CONFIG_REST_ACTIVE");
			
			var self = this;
			this.ajax("/api/admin/config/rest", "POST", RESTObject, 
					function() {
						MessageToast.show(self.getModel("i18n").getResourceBundle().getText("admin_notification_updated"));
					});	
		},
		onUpdateHangoutConfig: function() {
			var hangoutObject = {};
			var configModel = this.getModel("configModel");
			hangoutObject.KEY_CONFIG_HANGOUT_ACTIVE = configModel.getProperty("/hangout/KEY_CONFIG_HANGOUT_ACTIVE");
			
			var self = this;
			this.ajax("/api/admin/config/hangout", "POST", hangoutObject, 
					function() {
						MessageToast.show(self.getModel("i18n").getResourceBundle().getText("admin_notification_updated"));
					});	
		},
		onUpdateAndroidConfig: function() {
			var androidObject = {};
			var configModel = this.getModel("configModel");
			if (configModel.getProperty("/android/KEY_CONFIG_ANDROID_PROJECTID")) {
				androidObject.KEY_CONFIG_ANDROID_PROJECTID = configModel.getProperty("/android/KEY_CONFIG_ANDROID_PROJECTID");
			}
			if (configModel.getProperty("/android/KEY_CONFIG_ANDROID_PRIVATEKEYID")) {
				androidObject.KEY_CONFIG_ANDROID_PRIVATEKEYID = configModel.getProperty("/android/KEY_CONFIG_ANDROID_PRIVATEKEYID");
			}
			if (configModel.getProperty("/android/KEY_CONFIG_ANDROID_PRIVATEKEY")) {
				androidObject.KEY_CONFIG_ANDROID_PRIVATEKEY = configModel.getProperty("/android/KEY_CONFIG_ANDROID_PRIVATEKEY");
			}
			if (configModel.getProperty("/android/KEY_CONFIG_ANDROID_EMAIL")) {
				androidObject.KEY_CONFIG_ANDROID_EMAIL = configModel.getProperty("/android/KEY_CONFIG_ANDROID_EMAIL");
			}
			if (configModel.getProperty("/android/KEY_CONFIG_ANDROID_CLIENTID")) {
				androidObject.KEY_CONFIG_ANDROID_CLIENTID = configModel.getProperty("/android/KEY_CONFIG_ANDROID_CLIENTID");
			}
			if (configModel.getProperty("/android/KEY_CONFIG_ANDROID_CERTURL")) {
				androidObject.KEY_CONFIG_ANDROID_CERTURL = configModel.getProperty("/android/KEY_CONFIG_ANDROID_CERTURL");
			}
			androidObject.KEY_CONFIG_ANDROID_ACTIVE = configModel.getProperty("/android/KEY_CONFIG_ANDROID_ACTIVE");
			
			var self = this;
			this.ajax("/api/admin/config/android", "POST", androidObject, 
					function() {
						MessageToast.show(self.getModel("i18n").getResourceBundle().getText("admin_notification_updated"));
					});		
		}
	});
});