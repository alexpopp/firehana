sap.ui.define([
	"com/firehana/webapp/controller/BaseController",
	"sap/ui/model/json/JSONModel",
	"sap/m/MessageToast",
	"sap/m/Button",
	"sap/m/Dialog",
	"sap/m/Text",
	"sap/ui/model/resource/ResourceModel"
], function(BaseController, JSONModel, MessageToast, Button, Dialog, Text, ResourceModel) {
	"use strict";
	return BaseController.extend("com.firehana.webapp.controller.AdminProxy", {
		onInit: function() {
			var i18nModel = new ResourceModel({
				bundleName: "com.firehana.webapp.i18n.i18n"
			});
			this.setModel(i18nModel, "i18n");
	        
			var configModel = new JSONModel({});
			this.setModel(configModel, "configModel");

	        var self = this;
			this.ajax("/api/admin/config/proxy", "GET", {}, 
				function(result) {
					self.getModel("configModel").setProperty("/proxy", result);
				});
		},
		onUpdateProxyConfig: function() {
			var proxyObject = {};
			var configModel = this.getModel("configModel");
			if (configModel.getProperty("/proxy/KEY_CONFIG_PROXY_URL")) {
				proxyObject.KEY_CONFIG_PROXY_URL = configModel.getProperty("/proxy/KEY_CONFIG_PROXY_URL");
			}
			
			proxyObject.KEY_CONFIG_PROXY_ACTIVE = configModel.getProperty("/proxy/KEY_CONFIG_PROXY_ACTIVE");
			proxyObject.KEY_CONFIG_PROXY_SENDPW = configModel.getProperty("/proxy/KEY_CONFIG_PROXY_SENDPW");
			
			var self = this;
			this.ajax("/api/admin/config/proxy", "POST", proxyObject, 
						function() {
							MessageToast.show(self.getModel("i18n").getResourceBundle().getText("admin_notification_updated"));
						});
		}
	});
});