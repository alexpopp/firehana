sap.ui.define([
	"com/firehana/webapp/controller/BaseController",
	"sap/ui/model/json/JSONModel",
	"sap/suite/ui/commons/ProcessFlowNode",
	"sap/m/MessageToast",
	"sap/ui/model/resource/ResourceModel",
	"sap/suite/ui/commons/ProcessFlowConnectionLabel"
], function(BaseController, JSONModel, ProcessFlowNode, MessageToast, ResourceModel, ProcessFlowConnectionLabel) {
	"use strict";
	return BaseController.extend("com.firehana.webapp.controller.TriggerPage", {
		onInit: function() {
			var i18nModel = new ResourceModel({
				bundleName: "com.firehana.webapp.i18n.i18n"
			});
			this.setModel(i18nModel, "i18n");
			
			var viewModel = new JSONModel({
			activation: {
				availableTables: [],
				activeActivation: {},
				availableOperations: [{
					key: "INSERT",
					name: this.getModel("i18n").getResourceBundle().getText("activation_availableOperations_create")
				}, {
					key: "UPDATE",
					name: this.getModel("i18n").getResourceBundle().getText("activation_availableOperations_update")
				}, {
					key: "DELETE",
					name: this.getModel("i18n").getResourceBundle().getText("activation_availableOperations_delete")
				}]
			},
			filter: {
				availableColumns: [],
				activeFilter: {},
				availableComparators: [{
					key: "EQUAL",
					name: this.getModel("i18n").getResourceBundle().getText("filter_availableComparators_e")
				}, {
					key: "BIGGER",
					name: this.getModel("i18n").getResourceBundle().getText("filter_availableComparators_b")
				}, {
					key: "BIGGER_OR_EQUAL",
					name: this.getModel("i18n").getResourceBundle().getText("filter_availableComparators_be")
				}, {
					key: "SMALLER",
					name: this.getModel("i18n").getResourceBundle().getText("filter_availableComparators_s")
				}, {
					key: "SMALLER_OR_EQUAL",
					name: this.getModel("i18n").getResourceBundle().getText("filter_availableComparators_se")
				}]
			},
			timing: {
				activeTiming: {},
				availableTimings: [{
					key: "HOUR",
					name: this.getModel("i18n").getResourceBundle().getText("timing_availableTimings_hour")
				}, {
					key: "DAY",
					name: this.getModel("i18n").getResourceBundle().getText("timing_availableTimings_day")
				}, {
					key: "WEEK",
					name: this.getModel("i18n").getResourceBundle().getText("timing_availableTimings_week")
				}]
			},
			notification: {
				activeNotification: {},
				availableMediums: [],
				allMediums: [{
					key: "KEY_CONFIG_MAIL_ACTIVE",
					name: this.getModel("i18n").getResourceBundle().getText("notification_mail")
				},{
					key: "KEY_CONFIG_SLACK_ACTIVE",
					name: this.getModel("i18n").getResourceBundle().getText("notification_slack")
				},{
					key: "KEY_CONFIG_STRIDE_ACTIVE",
					name: this.getModel("i18n").getResourceBundle().getText("notification_stride")
				},{
					key: "KEY_CONFIG_TWILIO_ACTIVE",
					name: this.getModel("i18n").getResourceBundle().getText("notification_twilio")
				},{
					key: "KEY_CONFIG_AUDIT_ACTIVE",
					name: this.getModel("i18n").getResourceBundle().getText("notification_audit")
				},{
					key: "KEY_CONFIG_ANDROID_ACTIVE",
					name: this.getModel("i18n").getResourceBundle().getText("notification_android")
				},{
					key: "KEY_CONFIG_REST_ACTIVE",
					name: this.getModel("i18n").getResourceBundle().getText("notification_rest")
				},{
					key: "KEY_CONFIG_HANGOUT_ACTIVE",
					name: this.getModel("i18n").getResourceBundle().getText("notification_hangout")
				}]
			}
		});
			this.setModel(viewModel, "viewModel");
			
			var self = this;
			this.ajax("/api/trigger/util/tables", "GET", {},
				function(result) {
					self.getModel("viewModel").setProperty("/activation/availableTables", result);
				});
			this.ajax("/api/trigger/util/mediums", "GET", {},
				function(availableMediumKeys) {
					var allMediums = self.getModel("viewModel").getProperty("/notification/allMediums");
					var availableMediums = [];
					for (var i=0; i<availableMediumKeys.length; i++) {
						for (var j=0; j<allMediums.length; j++) {
							if (allMediums[j].key === availableMediumKeys[i]) {
								availableMediums.push(allMediums[j]);
							}
						}
					}
					self.getModel("viewModel").setProperty("/notification/availableMediums", availableMediums);
				});

			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.getRoute("trigger").attachPatternMatched(this._onObjectMatched, this);
		},
		_onObjectMatched: function(oEvent) {
			this.reload(oEvent.getParameter("arguments").triggerId);
			var triggerModel = new JSONModel({
				trigger: {},
				activation: {},
				timing: {},
				filters: [],
				notifications: []
			});

			this.setModel(triggerModel, "triggerModel");
		},
		reload: function(triggerId) {
			var self = this;
			var processModel = new JSONModel({
				lanes: [{
					"id": "0",
					"icon": "sap-icon://begin",
					"label": this.getModel("i18n").getResourceBundle().getText("trigger_process_activation"),
					"position": 0
				}, {
					"id": "1",
					"icon": "sap-icon://filter",
					"label": this.getModel("i18n").getResourceBundle().getText("trigger_process_filter"),
					"position": 1
				}, {
					"id": "2",
					"icon": "sap-icon://notification-2",
					"label": this.getModel("i18n").getResourceBundle().getText("trigger_process_notification"),
					"position": 2
				}, {
					"id": "3",
					"icon": "sap-icon://date-time",
					"label": this.getModel("i18n").getResourceBundle().getText("trigger_process_timing"),
					"position": 3
				}],
				nodes: []
			});
			this.setModel(processModel, "processModel");

			
			this.ajax("/api/trigger/trigger/" + triggerId, "GET", {},
				function(result) {
					result.trigger.mactivated = result.trigger.mactivated === 0 ? false : true;
					self.getModel("triggerModel").setProperty("/trigger", result.trigger);

					//Timing has always id=1 (see newNodes.length + 1)
					if (result.timing) {
						self.getModel("triggerModel").setProperty("/timing", result.timing);
						var title = "";
						if (result.timing.mimmediate) {
							title = self.getModel("i18n").getResourceBundle().getText("timing_immediate");	
						} else {
							var availableTimings = self.getModel("viewModel").getProperty("/timing/availableTimings");
							for (var i=0; i<availableTimings.length; i++) {
								if (availableTimings[i].key === result.timing.mtime) {
									title = availableTimings[i].name;
									break;
								}
							}	
						}
						self._addNodeToModel(3, title, [], "Positive", "Ok", [result.timing.mbundled ? "Bundled" : ""], "update",
							"timing", 0);
					} else {
						self._addNodeToModel(3, "", [], "Negative", "", [self.getModel("i18n").getResourceBundle().getText("trigger_process_timing_empty")], "create", "timing", 0);
					}

					var notificationPositions = [];
					if (result.notifications && result.notifications.length > 0) {
						self.getModel("triggerModel").setProperty("/notifications", result.notifications);
						for (var i = 0; i < result.notifications.length; i++) {
							notificationPositions.push(2 + i);
							var medium = "";
							var availableMediums = self.getModel("viewModel").getProperty("/notification/availableMediums");
							for (var j=0; j<availableMediums.length; j++) {
								if (availableMediums[j].key === result.notifications[i].mmedium) {
									medium = availableMediums[j].name;
									break;
								}
							}
							
							self._addNodeToModel(2, medium, [1], "Positive", "Ok", [result.notifications[i].mdetail1], "update", "notification", i);
						}
					}

					if (notificationPositions.length === 0) {
						self._addNodeToModel(2, "", [1], "Negative", "", [self.getModel("i18n").getResourceBundle().getText("trigger_process_notification_empty")], "create", "notification", -1);
					} else {
						self._addNodeToModel(2, "", notificationPositions, "Neutral", "", [self.getModel("i18n").getResourceBundle().getText("trigger_process_notification_additional")], "create", "notification", -1);
					}

					var filterPositions = [];
					if (result.filters && result.filters.length > 0) {
						self.getModel("triggerModel").setProperty("/filters", result.filters);
						for (var i = 0; i < result.filters.length; i++) {
							filterPositions.push(3 + result.notifications.length + i);
							var children = [2 + result.notifications.length];
							if (result.filters.length > 1) {
									var oConnectionLabel = sap.ui.getCore().byId("filterLabel" + i);
									if (!oConnectionLabel) {
										oConnectionLabel = new ProcessFlowConnectionLabel({
											id: "filterLabel" + i,
											enabled: false,
											icon: "sap-icon://multiselect-all",
											state: "Neutral",
											priority: 6
										});
									}
									children = [{
										nodeId: 2 + result.notifications.length,
										connectionLabel: oConnectionLabel
									}];
							}
							self._addNodeToModel(1, result.filters[i].mcolumn + " " + result.filters[i].mcomparator + " " + result.filters[i].mvalue, children, "Positive", "Ok", ["Data type: " + result.filters[i].mdatatype], "update", "filter", i);
						}
					}

					if (filterPositions.length === 0) {
						self._addNodeToModel(1, "", [2 + result.notifications.length], "Neutral", "", [self.getModel("i18n").getResourceBundle().getText("trigger_process_filter_empty")], "create", "filter", -1);
					} else {
						self._addNodeToModel(1, "", filterPositions, "Neutral", "", [self.getModel("i18n").getResourceBundle().getText("trigger_process_filter_additional")], "create", "filter", -1);
					}

					//Activation has the id=2+notifications.length+filters.length+1
					if (result.activation) {
						self.getModel("triggerModel").setProperty("/activation", result.activation);
						self._addNodeToModel(0, result.activation.moperation + " on " + result.activation.mtable, [3 + result.notifications.length + filterPositions.length], "Positive",
							"Ok", ["Schema: " + result.activation.mschema], "update", "activation", 0);
					} else {
						self._addNodeToModel(0, "", [3 + result.notifications.length + filterPositions.length], "Negative", "", [self.getModel("i18n").getResourceBundle().getText("trigger_process_activation_empty")],
							"create", "activation", 0);
					}

					self.getView().byId("processflow1").updateModel();
					self.getView().byId("processflow1").setShowLabels(true);
					self.getView().byId("processflow1").optimizeLayout(true);
				});
		},
		_addNodeToModel: function(lane, title, children, state, stateText, texts, actionType, nodeType, index) {
			var newNodes = this.getModel("processModel").getProperty("/nodes");
			var node = {
				"id": newNodes.length + 1,
				"lane": lane,
				"title": title,
				"children": children,
				"state": state,
				"stateText": stateText,
				"texts": texts,
				"actionType": actionType,
				"nodeType": nodeType,
				"index": index
			};
			newNodes.push(node);

			this.getModel("processModel").setProperty("/nodes", newNodes);
		},
		createProcessNode: function(sId, oContext) {
			var oNode;
			var actionType = oContext.getProperty("actionType");
			if (actionType && actionType === "create") {
				oNode = new ProcessFlowNode(sId, {
					laneId: "{processModel>lane}",
					nodeId: "{processModel>id}",
					children: "{processModel>children}",
					texts: "{processModel>texts}",
					type: "Aggregated",
					state: "{processModel>state}"
				});
			} else {
				oNode = new ProcessFlowNode(sId, {
					laneId: "{processModel>lane}",
					nodeId: "{processModel>id}",
					title: "{processModel>title}",
					children: "{processModel>children}",
					texts: "{processModel>texts}",
					state: "{processModel>state}",
					stateText: "{processModel>stateText}",
					focused: true
				});
			}

			return oNode;
		},
		formatConnectionLabels: function(childrenData) {
			var aChildren = [];
			for (var i = 0; childrenData &&  i < childrenData.length; i++) {
				if (childrenData[i].connectionLabel && childrenData[i].connectionLabel.id) {
					var oConnectionLabel = sap.ui.getCore().byId(childrenData[i].connectionLabel.id);
					if (!oConnectionLabel) {
						oConnectionLabel = new ProcessFlowConnectionLabel({
							id: childrenData[i].connectionLabel.id,
							text: childrenData[i].connectionLabel.text,
							enabled: childrenData[i].connectionLabel.enabled,
							icon: childrenData[i].connectionLabel.icon,
							state: childrenData[i].connectionLabel.state,
							priority: childrenData[i].connectionLabel.priority
						});
					}
					aChildren.push({
						nodeId: childrenData[i].nodeId,
						connectionLabel: oConnectionLabel
					});
				} else if (jQuery.type(childrenData[i]) === 'number'){
					aChildren.push(childrenData[i]);
				}
			}
			return aChildren;
		},
		onNodePress: function(event) {
			var allNodes = this.getModel("processModel").getProperty("/nodes");
			for (var i = 0; i < allNodes.length; i++) {
				if (allNodes[i].id + "" === event.getParameters().getNodeId()) {
					switch (allNodes[i].nodeType) {
						case "activation":
							var activeActivation = this.getModel("triggerModel").getProperty("/activation");
							this.getModel("viewModel").setProperty("/activation/activeActivation", activeActivation);
							this._onOpenDialog("createActivationDialog", "com.firehana.webapp.view.component.CreateActivationDialog");
							break;
						case "filter":
							var activeFilter = this.getModel("triggerModel").getProperty("/filters/" + allNodes[i].index);
							if (!activeFilter) {
								activeFilter = {};
							}

							var activation = this.getModel("triggerModel").getProperty("/activation");
							if (activation.mtable === undefined && !activeFilter.mkey) {
								MessageToast.show(this.getModel("i18n").getResourceBundle().getText("filter_error_activationFirst"));
								return;
							}

							var self = this;
							this.ajax("/api/trigger/util/tables/" + activation.mschema + "/" + activation.mtable, "GET", {},
								function(result) {
									self.getModel("viewModel").setProperty("/filter/availableColumns", result);
								});

							this.getModel("viewModel").setProperty("/filter/activeFilter", activeFilter);
							this._onOpenDialog("createFilterDialog", "com.firehana.webapp.view.component.CreateFilterDialog");
							break;
						case "notification":
							var activeNotification = Object.assign({}, this.getModel("triggerModel").getProperty("/notifications/" + allNodes[i].index)); //objet.asign to avoid changing triggerModel from UI
							if (!activeNotification.mkey) {
								activeNotification = {};
								
								if (this.getModel("viewModel").getProperty("/notification/availableMediums").length > 0) { //if there is at least on medium, select the first one per default
										activeNotification.mmedium = this.getModel("viewModel").getProperty("/notification/availableMediums")[0].key;
								}
							}
							this.getModel("viewModel").setProperty("/notification/activeNotification", activeNotification);
							this._onOpenDialog("createNotificationDialog", "com.firehana.webapp.view.component.CreateNotificationDialog");
							break;
						case "timing":
							var activeTiming = this.getModel("triggerModel").getProperty("/timing");
							if (!activeTiming.mkey) {
								activeTiming.mimmediate = true; //setting default
								activeTiming.mbundled = false; //setting default
							}
							this.getModel("viewModel").setProperty("/timing/activeTiming", activeTiming);
							this._onOpenDialog("createTimingDialog", "com.firehana.webapp.view.component.CreateTimingDialog");
							break;
						default:
					}

					break;
				}
			}
		},
		_onOpenDialog: function(viewId, xmlName) {
			var oView = this.getView();
			var oDialog = oView.byId(viewId);
			if (!oDialog) {
				oDialog = sap.ui.xmlfragment(oView.getId(), xmlName, this);
				oView.addDependent(oDialog);
			}

			oDialog.open();
		},
		_onCloseDialog: function(event) {
			var dialogId = event.getSource().data("dialogId");
			if (dialogId) {
				this.getView().byId(dialogId).close();
			}
		},
		onCreateOrUpdateActivation: function() {
			var triggerId = this.getModel("triggerModel").getProperty("/trigger/mkey");
			var operation = this.getModel("viewModel").getProperty("/activation/activeActivation/moperation");
			var tableSchema = this.getView().byId("inputTable").getSelectedKey().split("%firehana%");
			var schema = tableSchema[0];
			var table = tableSchema[1];

			if (schema === undefined || schema.length < 1 || table === undefined || table.length < 1) {
				this.showErrorDialog(this.getModel("i18n").getResourceBundle().getText("activation_error_table"));
				return;
			}
			if (operation === undefined || operation.length < 1) {
				this.showErrorDialog(this.getModel("i18n").getResourceBundle().getText("activation_error_operation"));
				return;
			}

			var activationKey = this.getModel("viewModel").getProperty("/activation/activeActivation/mkey");
			var self = this;
			if (activationKey) {
				this.ajax("/api/trigger/activation/" + activationKey, "PUT", {
						schema: schema,
						table: table,
						operation: operation
					},
					function() {
						self.getView().byId("createActivationDialog").close();
						MessageToast.show("Updated activation successfully!");
						self.reload(triggerId);
					});
			} else {
				this.ajax("/api/trigger/activation", "POST", {
						schema: schema,
						table: table,
						operation: operation,
						triggerId: triggerId
					},
					function() {
						self.getView().byId("createActivationDialog").close();
						MessageToast.show("Created activation successfully!");
						self.reload(triggerId);
					});
			}
		},
		onDeleteActivation: function() {
			var activationKey = this.getModel("viewModel").getProperty("/activation/activeActivation/mkey");
			var triggerId = this.getModel("triggerModel").getProperty("/trigger/mkey");
			var self = this;
			this.ajax("/api/trigger/activation/" + activationKey, "DELETE", {},
				function() {
					self.getView().byId("createActivationDialog").close();
					MessageToast.show("Deleted activation successfully!");
					self.reload(triggerId);
				});
		},
		onCreateOrUpdateTiming: function() {
			var immediate = this.getModel("viewModel").getProperty("/timing/activeTiming/mimmediate");
			var time = this.getModel("viewModel").getProperty("/timing/activeTiming/mtime");
			var bundled = this.getModel("viewModel").getProperty("/timing/activeTiming/mbundled");
			var triggerId = this.getModel("triggerModel").getProperty("/trigger/mkey");
			
			if (!immediate && !time) {
				this.showErrorDialog(this.getModel("i18n").getResourceBundle().getText("timing_error_frequency"));
				return;
			}

			var timingKey = this.getModel("viewModel").getProperty("/timing/activeTiming/mkey");
			var self = this;
			if (timingKey) {
				this.ajax("/api/trigger/timing/" + timingKey, "PUT", {
						immediate: immediate,
						time: time,
						bundled: bundled
					},
					function() {
						self.getView().byId("createTimingDialog").close();
						MessageToast.show("Updated timing successfully!");
						self.reload(triggerId);
					});
			} else {
				this.ajax("/api/trigger/timing", "POST", {
						immediate: immediate,
						time: time,
						bundled: bundled,
						triggerId: triggerId
					},
					function() {
						self.getView().byId("createTimingDialog").close();
						MessageToast.show("Created new timing successfully!");
						self.reload(triggerId);
					});
			}
		},
		onDeleteTiming: function() {
			var timingKey = this.getModel("viewModel").getProperty("/timing/activeTiming/mkey");
			var triggerId = this.getModel("triggerModel").getProperty("/trigger/mkey");
			var self = this;
			this.ajax("/api/trigger/timing/" + timingKey, "DELETE", {},
				function() {
					self.getView().byId("createTimingDialog").close();
					MessageToast.show("Timing deleted successfully!");
					self.reload(triggerId);
				});
		},
		onCreateOrUpdateFilter: function() {
			var triggerId = this.getModel("triggerModel").getProperty("/trigger/mkey");
			var column = this.getModel("viewModel").getProperty("/filter/activeFilter/mcolumn");
			var comparator = this.getModel("viewModel").getProperty("/filter/activeFilter/mcomparator");
			var value = this.getModel("viewModel").getProperty("/filter/activeFilter/mvalue");
			var dataType = "";

			if (column === undefined || column.length < 1) {
				this.showErrorDialog(this.getModel("i18n").getResourceBundle().getText("filter_error_column"));
				return;
			}
			if (comparator === undefined || comparator.length < 1) {
				this.showErrorDialog(this.getModel("i18n").getResourceBundle().getText("filter_error_comparator"));
				return;
			}
			if (value === undefined || value.length < 1) {
				this.showErrorDialog(this.getModel("i18n").getResourceBundle().getText("filter_error_value"));
				return;
			}

			var availableColumns = this.getModel("viewModel").getProperty("/filter/availableColumns");
			for (var i = 0; i < availableColumns.length; i++) {
				if (availableColumns[i].COLUMN_NAME === column) {
					if (availableColumns[i].DATA_TYPE_NAME !== "BOOLEAN" && value.length > availableColumns[i].LENGTH) {
						this.showErrorDialog(this.getModel("i18n").getResourceBundle().getText("filter_error_tooLong"));
						return;
					}
					switch (availableColumns[i].DATA_TYPE_NAME) {
						case "INTEGER":
							if (!Number.isInteger(Number(value))) {
								this.showErrorDialog(this.getModel("i18n").getResourceBundle().getText("filter_error_noInteger"));
								return;
							}
							break;
						case "BOOLEAN":
							if (value.toLowerCase() !== "true" && value.toLowerCase() !== "false") {
								this.showErrorDialog(this.getModel("i18n").getResourceBundle().getText("filter_error_noBoolean"));
								return;
							}
					}

					dataType = availableColumns[i].DATA_TYPE_NAME;
				}
			}

			var filterKey = this.getModel("viewModel").getProperty("/filter/activeFilter/mkey");
			var self = this;
			if (filterKey) {
				this.ajax("/api/trigger/filter/" + filterKey, "PUT", {
						column: column,
						comparator: comparator,
						value: value,
						dataType: dataType
					},
					function() {
						self.getView().byId("createFilterDialog").close();
						MessageToast.show("Updated filter successfully!");
						self.reload(triggerId);
					});
			} else {
				this.ajax("/api/trigger/filter", "POST", {
						column: column,
						comparator: comparator,
						value: value,
						triggerId: triggerId,
						dataType: dataType
					},
					function() {
						self.getView().byId("createFilterDialog").close();
						MessageToast.show("Created new filter successfully!");
						self.reload(triggerId);
					});
			}
		},
		onDeleteFilter: function() {
			var filterKey = this.getModel("viewModel").getProperty("/filter/activeFilter/mkey");
			var triggerId = this.getModel("triggerModel").getProperty("/trigger/mkey");
			var self = this;
			this.ajax("/api/trigger/filter/" + filterKey, "DELETE", {},
				function() {
					self.getView().byId("createFilterDialog").close();
					MessageToast.show("Filter deleted successfully!");
					self.reload(triggerId);
				});
		},
		onCreateOrUpdateNotification: function() {
			var medium = this.getModel("viewModel").getProperty("/notification/activeNotification/mmedium");
			var text = this.getModel("viewModel").getProperty("/notification/activeNotification/mtext");
			var details1 = this.getModel("viewModel").getProperty("/notification/activeNotification/mdetail1");
			var details2 = this.getModel("viewModel").getProperty("/notification/activeNotification/mdetail2");
			var triggerId = this.getModel("triggerModel").getProperty("/trigger/mkey");
			
			if (medium === undefined || medium.length < 1) {
				this.showErrorDialog(this.getModel("i18n").getResourceBundle().getText("notification_error_medium"));
				return;
			}

			var notificationKey = this.getModel("viewModel").getProperty("/notification/activeNotification/mkey");
			var self = this;
			if (notificationKey) {
				this.ajax("/api/trigger/notification/" + notificationKey, "PUT", {
						medium: medium,
						text: text,
						detail1: details1,
						detail2: details2
					},
					function() {
						self.getView().byId("createNotificationDialog").close();
						MessageToast.show("Updated notification successfully!");
						self.reload(triggerId);
					});
			} else {
				this.ajax("/api/trigger/notification", "POST", {
						medium: medium,
						triggerId: triggerId,
						text: text,
						detail1: details1,
						detail2: details2
					},
					function() {
						self.getView().byId("createNotificationDialog").close();
						MessageToast.show("Created new notification successfully!");
						self.reload(triggerId);
					});
			}
		},
		onDeleteNotification: function() {
			var notificationKey = this.getModel("viewModel").getProperty("/notification/activeNotification/mkey");
			var triggerId = this.getModel("triggerModel").getProperty("/trigger/mkey");
			var self = this;
			this.ajax("/api/trigger/notification/" + notificationKey, "DELETE", {},
				function() {
					self.getView().byId("createNotificationDialog").close();
					MessageToast.show("Notification deleted successfully!");
					self.reload(triggerId);
				});
		},
		onTriggerStateChange: function(event) {
			var triggerId = this.getModel("triggerModel").getProperty("/trigger/mkey");
			var newStatus = event.getParameters().state;
			
			if (!this.getModel("triggerModel").getProperty("/timing/mkey") || !this.getModel("triggerModel").getProperty("/activation/mkey") || !this.getModel("triggerModel").getProperty("/notifications").length === 0) {
				this.showErrorDialog(this.getModel("i18n").getResourceBundle().getText("trigger_activate_error"), this.getModel("i18n").getResourceBundle().getText("trigger_activate_error_title"));
				this.getModel("triggerModel").setProperty("/trigger/mactivated", !newStatus);
				return;
			}

			var self = this;
			this.ajax("/api/trigger/trigger/" + triggerId, "PUT", {
					active: newStatus
				},
				function() {
					self.getModel("triggerModel").setProperty("/trigger/mactivated", newStatus);
					self.getModel("triggerModel").setProperty("/trigger/mstatus", newStatus ? 1 : 0);
					var eventBus = sap.ui.getCore().getEventBus();
					eventBus.publish("UPDATE", "UPDATE_TRIGGER", self.getModel("triggerModel").getProperty("/trigger"));
					MessageToast.show("Set trigger status to " + newStatus);
				},
				function() {
					self.getModel("triggerModel").setProperty("/trigger/mactivated", !newStatus);
				});
		},
		onDeleteTrigger: function() {
			var self = this;
			this.showConfirmDialog(this.getModel("i18n").getResourceBundle().getText("trigger_delete_confirm"), undefined, function() {
				var triggerId = self.getModel("triggerModel").getProperty("/trigger/mkey");
				self.ajax("/api/trigger/trigger/" + triggerId, "DELETE", {},
					function() {
						var eventBus = sap.ui.getCore().getEventBus();
						eventBus.publish("UPDATE", "DELETE_TRIGGER", triggerId);
						self.onNavBack();
					});	
			});
		}

	});
});