sap.ui.define([
   "com/firehana/webapp/controller/BaseController",
   "sap/ui/model/json/JSONModel",
   "sap/ui/model/Filter"
], function (BaseController, JSONModel, Filter) {
   "use strict";
   return BaseController.extend("com.firehana.webapp.controller.TriggerList", {
		onInit : function () {
			var eventBus = sap.ui.getCore().getEventBus();
			eventBus.subscribe("UPDATE", "NEW_TRIGGER", this.addTrigger, this);
			eventBus.subscribe("UPDATE", "UPDATE_TRIGGER", this.updateTrigger, this);
			eventBus.subscribe("UPDATE", "DELETE_TRIGGER", this.deleteTrigger, this);
			
			this._oTable = this.byId("triggerTable");
			var viewModel = new JSONModel({
				tableBusyDelay: 0,
				inStock: 0,
				shortage: 0,
				outOfStock: 0,
				countAll: 0,
				busy:true
			});
			var triggerListModel = new JSONModel({
				list: []
			});
			
			this.setModel(viewModel, "viewModel");
			this.setModel(triggerListModel, "triggerListModel");
			
			this._mFilters = {
				"countActive": [new Filter("mstatus", "EQ", 1)],
				"countInactive": [new Filter("mstatus", "EQ", 0)],
				"countStopped": [new Filter("mstatus", "EQ", 2)],
				"countAll": []
			};
			
			this.ajax("/api/trigger/trigger", "GET", {}, 
				function(result) {
					triggerListModel.setProperty("/list", result);
					viewModel.setProperty("/busy", false);	
				}, function() {
					viewModel.setProperty("/busy", false);		
				}, true);
		},
		addTrigger: function(channel, event, data) {
			var triggerList = this.getModel("triggerListModel").getProperty("/list");
			triggerList.push(data);
			this.getModel("triggerListModel").setProperty("/list", triggerList);
		},
		deleteTrigger: function(channel, event, data) {
			var triggerList = this.getModel("triggerListModel").getProperty("/list");
			var newTriggerList = [];
			for (var i=0; i<triggerList.length; i++) {
				if (triggerList[i].mkey !== data) {
					newTriggerList.push(triggerList[i]);	
				}
			}
			this.getModel("triggerListModel").setProperty("/list", newTriggerList);
		},
		updateTrigger: function(channel, event, data) {
			var triggerList = this.getModel("triggerListModel").getProperty("/list");
			for (var i=0; i<triggerList.length; i++) {
				if (triggerList[i].mkey === data.mkey) {
					triggerList[i] = data;
				}
			}
			this.getModel("triggerListModel").setProperty("/list", triggerList);	
		},
		onUpdateFinished: function(oEvent) {
		   // update the worklist's object counter after the table update
		   var oTable = oEvent.getSource();
		   var viewModel = this.getModel("viewModel"),
		
		   iTotalItems = oEvent.getParameter("total");
		   // only update the counter if the length is final and
		   // the table is not empty
		   if (iTotalItems && oTable.getBinding("items").isLengthFinal()) {
		      var allTriggers = this.getModel("triggerListModel").getProperty("/list");
		      var countActive = 0;
		      var countStopped = 0;
		      var countInactive = 0;
		      for (var i=0; i<allTriggers.length; i++) {
		      	if (allTriggers[i].mstatus === 0) {
		      		countInactive++;	
		      	} else if (allTriggers[i].mstatus === 1) {
		      		countActive++;	
		      	} else {
		      		countStopped++;	
		      	}
		      }
		      
		      viewModel.setProperty("/countAll", allTriggers.length);
		      viewModel.setProperty("/countActive", countActive);
		      viewModel.setProperty("/countInactive", countInactive);
		      viewModel.setProperty("/countStopped", countStopped);
		   } 
		},
		onQuickFilter: function(oEvent) {
		   var oBinding = this._oTable.getBinding("items"),
		      sKey = oEvent.getParameter("selectedKey");
		   oBinding.filter(this._mFilters[sKey]);
		},
		onTriggerPressed: function(oEvent) {
			this.getRouter().navTo("trigger", {
				triggerId: oEvent.getSource().getBindingContext("triggerListModel").getProperty("mkey")
			});
		}
   });
});