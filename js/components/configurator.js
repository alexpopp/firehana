/*eslint no-console: 0, no-unused-vars: 0*/
"use strict";
var dbaccess = require("../utils/dbaccess");
var C = require("../utils/constants");
var async = require("async");
var log = require("../utils/log");

/*
	These arrays define all config entries that should be editable from the administration page. If "secure === true" than it ill be saved in secure store, otherwise in the Config table in plain text.
*/
var mailKeys = [{key: C.KEY_CONFIG_MAIL_ACTIVE, secure: false}, {key: C.KEY_CONFIG_MAIL_PORT, secure: false}, {key: C.KEY_CONFIG_MAIL_HOST, secure: false}, {key: C.KEY_CONFIG_MAIL_USERNAME, secure: false}, {key: C.KEY_CONFIG_MAIL_TLS, secure: false}, {key: C.KEY_CONFIG_MAIL_REJECTUNAUTH, secure: false}, {key: C.KEY_CONFIG_MAIL_FROM, secure: false}, {key: C.KEY_CONFIG_MAIL_PASSWORD, secure: true}];
var slackKeys = [{key: C.KEY_CONFIG_SLACK_HOOK, secure: false}, {key: C.KEY_CONFIG_SLACK_ACTIVE, secure: false}];
var strideKeys = [{key: C.KEY_CONFIG_STRIDE_ACTIVE, secure: false}];
var twilioKeys = [{key: C.KEY_CONFIG_TWILIO_SID, secure: false}, {key: C.KEY_CONFIG_TWILIO_AUTHTOKEN, secure: true}, {key: C.KEY_CONFIG_TWILIO_ACTIVE, secure: false}];
var auditKeys = [{key: C.KEY_CONFIG_AUDIT_ACTIVE, secure: false}];
var androidKeys = [{key: C.KEY_CONFIG_ANDROID_PROJECTID, secure: false}, {key: C.KEY_CONFIG_ANDROID_PRIVATEKEYID, secure: false}, {key: C.KEY_CONFIG_ANDROID_PRIVATEKEY, secure: true}, {key: C.KEY_CONFIG_ANDROID_EMAIL, secure: false}, {key: C.KEY_CONFIG_ANDROID_CLIENTID, secure: false}, {key: C.KEY_CONFIG_ANDROID_CERTURL, secure: false}, {key: C.KEY_CONFIG_ANDROID_ACTIVE, secure: false}];
var restKeys = [{key: C.KEY_CONFIG_REST_ACTIVE, secure: false}];
var hangoutKeys = [{key: C.KEY_CONFIG_HANGOUT_ACTIVE, secure: false}];
var proxyKeys = [{key: C.KEY_CONFIG_PROXY_ACTIVE, secure: false}, {key: C.KEY_CONFIG_PROXY_URL, secure: false}, {key: C.KEY_CONFIG_PROXY_SENDPW, secure: false}];


/*
	Queries the Config table and secure store for all entries in "array".
*/
function getAllConfigEntries(db, array, callback) {
	var resultObject = {};
	async.forEach(array, function (entry, cb){
		if (entry.secure) {
			dbaccess.secureStore_retrieve(db, entry.key, function(error, result) {
				if (result) {
		    		resultObject[entry.key] = result;	
		    	}
		    	cb();
			});	
		} else {
			dbaccess.getConfig(db, entry.key, function(error, value) {
				if (value) {
		    		resultObject[entry.key] = value;	
		    	}
		    	cb();	
			}); 	
		}
	}, function(err) {
	    callback(err, resultObject);
	}); 	
}

/*
	Saves or updates or all entries in "array" at the secure store or the Config table.
*/
function updateAllConfigEntries(db, array, object, callback) {
	async.forEach(array, function (entry, cb){ 
		if (object[entry.key]) {
			if (entry.secure) {
				dbaccess.secureStore_insertOrUpdate(db, entry.key, object[entry.key], cb);        	
			} else {
				dbaccess.saveOrUpdateConfig(db, entry.key, object[entry.key], "", cb);
			}
		} else {
			cb();
		}
	}, function(err) {
	    callback(err);
	}); 	
}

module.exports = {
	getProxyConfig: function(db, callback) {
		getAllConfigEntries(db, proxyKeys, callback);
	},
	updateProxyConfig: function(db, object, callback) {
		updateAllConfigEntries(db, proxyKeys, object, callback);
	},
	getMailConfig: function(db, callback) {
		getAllConfigEntries(db, mailKeys, callback);
	},
	updateMailConfig: function(db, object, callback) {
		updateAllConfigEntries(db, mailKeys, object, callback);
	},
	getSlackConfig: function(db, callback) {
		getAllConfigEntries(db, slackKeys, callback);
	},
	updateSlackConfig: function(db, object, callback) {
		updateAllConfigEntries(db, slackKeys, object, callback);
	},
	getStrideConfig: function(db, callback) {
		getAllConfigEntries(db, strideKeys, callback);
	},
	updateStrideConfig: function(db, object, callback) {
		updateAllConfigEntries(db, strideKeys, object, callback);
	},
	getTwilioConfig: function(db, callback) {
		getAllConfigEntries(db, twilioKeys, callback);
	},
	updateTwilioConfig: function(db, object, callback) {
		updateAllConfigEntries(db, twilioKeys, object, callback);
	},
	getAuditConfig: function(db, callback) {
		getAllConfigEntries(db, auditKeys, callback);
	},
	updateAuditConfig: function(db, object, callback) {
		updateAllConfigEntries(db, auditKeys, object, callback);
	},
	getAndroidConfig: function(db, callback) {
		getAllConfigEntries(db, androidKeys, callback);
	},
	updateAndroidConfig: function(db, object, callback) {
		console.log("update android config: " + JSON.stringify(object));
		updateAllConfigEntries(db, androidKeys, object, callback);
	},
	getRESTConfig: function(db, callback) {
		getAllConfigEntries(db, restKeys, callback);
	},
	updateRESTConfig: function(db, object, callback) {
		updateAllConfigEntries(db, restKeys, object, callback);
	},
	getHangoutConfig: function(db, callback) {
		getAllConfigEntries(db, hangoutKeys, callback);
	},
	updateHangoutConfig: function(db, object, callback) {
		updateAllConfigEntries(db, hangoutKeys, object, callback);
	}
};

