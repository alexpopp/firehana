/*eslint no-console: 0, no-unused-vars: 0*/
"use strict";
var dbaccess = require("../utils/dbaccess");
var C = require("../utils/constants");
var configurator = require("./configurator");
var log = require("../utils/log");
var externalConnections = require("../utils/externalConnections");

var IncomingWebhook = require('@slack/client').IncomingWebhook;
var request = require('request');


function sendToProxy(db, proxyUrl, body, callback) {
		request({
		    url: proxyUrl,
		    method: "POST",
		    json: true,
		    body: body
		}, function (error, response){
			var resultError;
			if (error) {
		    	log.error(db, "fireproxy mail request", error);
		    	resultError = error;
		    } else if (response.statusCode !== 200) {
	    		log.error(db, "fireproxy request", response.body);	
	    		resultError = response.body;
		    } else {
		        log.debug("fireproxy success: " + JSON.stringify(response));
		    }
		    
		    if (callback) {
		    	callback(resultError);
		    }
		});		
}


function sendMail(db, text, to, subject, host, port, secure, rejectUnauthorized, from, user, password, proxyConfig, callback) {
	if (proxyConfig[C.KEY_CONFIG_PROXY_ACTIVE]) {
		var body = {
		    	text: text,
		    	to: to,
		    	subject: subject,
		    	host: host,
		    	port: port,
		    	secure: secure,
		    	rejectUnauthorized: rejectUnauthorized,
		    	from: from,
		    	user: user,
		    	password:  proxyConfig[C.KEY_CONFIG_PROXY_SENDPW] ? password : undefined
		    };	
	    sendToProxy(db, proxyConfig[C.KEY_CONFIG_PROXY_URL] + "/fireproxy/mail", body, callback);
	} else {
		externalConnections.sendMail(text, subject, to, host, port, secure, rejectUnauthorized, from, user, password, function(error, info) {
			if (error) {
				log.error(db, "connector.sendMail externalConnections.sendMail", error);	
			} else {
				log.debug("sendMail SUCCESS " + info);	
			}
			
			if (callback) { callback(error); }
		});	
	}	
}

function sendSlack(db, text, channel, webhookUrl, proxyConfig, callback) {
	var username = "FIREHANA";
	if (proxyConfig[C.KEY_CONFIG_PROXY_ACTIVE]) {
		var body = {
		    	text: text,
		    	channel: channel,
		    	webhookUrl: webhookUrl,
		    	username: username
		    };	
	    sendToProxy(db, proxyConfig[C.KEY_CONFIG_PROXY_URL] + "/fireproxy/slack", body, callback);
	} else {
		externalConnections.sendSlack(text, channel, webhookUrl, username, function(error, info) {
			if (error) {
				log.error(db, "connector.sendSlack externalConnections.sendSlack", error);	
			} else {
				log.debug("sendSlack SUCCESS " + info);	
			}
			
			if (callback) { callback(error); }
		});	
	}	
}

function sendStride(db, text, conversationURL, accessToken, proxyConfig, callback) {
	if (proxyConfig[C.KEY_CONFIG_PROXY_ACTIVE]) {
		var body = {
		    	text: text,
		    	conversationURL: conversationURL,
		    	accessToken: accessToken
		    };	
	    sendToProxy(db, proxyConfig[C.KEY_CONFIG_PROXY_URL] + "/fireproxy/stride", body, callback);
	} else {
		externalConnections.sendStride(text, conversationURL, accessToken, function(error, info) {
			if (error) {
				log.error(db, "connector.sendStride externalConnections.sendStride", error);	
			} else {
				log.debug("sendStride SUCCESS " + info);	
			}
			
			if (callback) { callback(error); }
		});	
	}
}

function sendTwilio(db, text, toNumber, accountSid, authToken, proxyConfig, callback) {
	if (proxyConfig[C.KEY_CONFIG_PROXY_ACTIVE]) {
		var body = {
		    	text: text,
		    	toNumber: toNumber,
		    	accountSid: accountSid,
		    	authToken: proxyConfig[C.KEY_CONFIG_PROXY_SENDPW] ? authToken : undefined
		    };	
	    sendToProxy(db, proxyConfig[C.KEY_CONFIG_PROXY_URL] + "/fireproxy/twilio", body, callback);
	} else {
		externalConnections.sendTwilio(text, accountSid, authToken, toNumber, function(error, info) {
			if (error) {
				log.error(db, "connector.sendTwilio externalConnections.sendTwilio", error);	
			} else {
				log.debug("sendTwilio SUCCESS " + info);	
			}
			
			if (callback) { callback(error); }
		});	
	}
}

function sendAndroidPush(db, topic, text, fb_project_id, fb_private_key_id, fb_private_key, fb_client_email, fb_client_id, fb_client_x509_cert_url, proxyConfig, callback) {
	if (proxyConfig[C.KEY_CONFIG_PROXY_ACTIVE]) {
		var body = {
		    	topic: topic,
		    	text: text,
		    	fb_project_id: fb_project_id,
		    	fb_private_key_id: fb_private_key_id,
		    	fb_private_key: proxyConfig[C.KEY_CONFIG_PROXY_SENDPW] ? fb_private_key : undefined,
		    	fb_client_email: fb_client_email,
		    	fb_client_id: fb_client_id,
		    	fb_client_x509_cert_url: fb_client_x509_cert_url
		    };	
		    
	    sendToProxy(db, proxyConfig[C.KEY_CONFIG_PROXY_URL] + "/fireproxy/android", body, callback);
	} else {
		externalConnections.sendAndroidPush(topic, text, fb_project_id, fb_private_key_id, fb_private_key, fb_client_email, fb_client_id, fb_client_x509_cert_url, function(error, info) {
			if (error) {
				log.error(db, "connector.sendAndroidPush externalConnections.sendAndroidPush", error);	
			} else {
				log.debug("sendAndroidPush SUCCESS " + info);	
			}
			
			if (callback) { callback(error); }
		});	
	}
}

function sendAuditLog(db, text, label, callback) {
	log.audit(db, text + " (" + label + ")");
		if (callback) { callback(undefined); }
}

function sendREST(db, text, url, method, proxyConfig, callback) {
	if (proxyConfig[C.KEY_CONFIG_PROXY_ACTIVE]) {
		var body = {
		    	text: text,
		    	url: url,
		    	method: method
		    };	
	    sendToProxy(db, proxyConfig[C.KEY_CONFIG_PROXY_URL] + "/fireproxy/rest", body, callback);
	} else {
		externalConnections.sendREST(text, url, method, function(error, info) {
			if (error) {
				log.error(db, "connector.sendREST externalConnections.sendREST", error);	
			} else {
				log.debug("sendREST SUCCESS " + info);	
			}
			
			if (callback) { callback(error); }
		});	
	}
}

function sendHangoutChat(db, text, webhookUrl, proxyConfig, callback) {
	if (proxyConfig[C.KEY_CONFIG_PROXY_ACTIVE]) {
		var body = {
		    	text: text,
		    	webhookUrl: webhookUrl
		    };	
	    sendToProxy(db, proxyConfig[C.KEY_CONFIG_PROXY_URL] + "/fireproxy/hangout", body, callback);
	} else {
		externalConnections.sendHangoutChat(text, webhookUrl, function(error, info) {
			if (error) {
				log.error(db, "connector.sendHangoutChat externalConnections.sendHangoutChat", error);	
			} else {
				log.debug("sendHangoutChat SUCCESS " + info);	
			}
			
			if (callback) { callback(error); }
		});	
	}
}

/*
	Decides based on "medium" where a notification should be sent to
*/
function splitEvent(db, text, detail1, detail2, detail3, medium) {
	configurator.getProxyConfig(db, function(error0, proxyConfig) {
		if (error0) {
			log.error(db, "connector.splitEvent getProxyConfig", error0);
			return;
		}
		switch(medium) {
			case C.KEY_CONFIG_MAIL_ACTIVE:
				configurator.getMailConfig(db, function(error, mailConfig) {
					if (error) {
						log.error(db, "connector.splitEvent getMailConfig", error);
						return;
					}
					var host = mailConfig[C.KEY_CONFIG_MAIL_HOST];
					var port = mailConfig[C.KEY_CONFIG_MAIL_PORT];
					var secure = mailConfig[C.KEY_CONFIG_MAIL_TLS];
					var rejectUnauthorized = mailConfig[C.KEY_CONFIG_MAIL_REJECTUNAUTH];
					var from = mailConfig[C.KEY_CONFIG_MAIL_FROM];
					var user = mailConfig[C.KEY_CONFIG_MAIL_USERNAME];
					var password = mailConfig[C.KEY_CONFIG_MAIL_PASSWORD];
				
					sendMail(db, text, detail1, detail2, host, port, secure, rejectUnauthorized, from, user, password, proxyConfig);
				});
				break;
			case C.KEY_CONFIG_SLACK_ACTIVE:
				configurator.getSlackConfig(db, function(error, slackConfig) {
					if (error) {
						log.error(db, "connector.splitEvent getSlackConfig", error);
						return;
					}
					var webhookUrl = slackConfig[C.KEY_CONFIG_SLACK_HOOK];

					sendSlack(db, text, detail1, webhookUrl, proxyConfig);
				});
				break;
			case C.KEY_CONFIG_STRIDE_ACTIVE:
				configurator.getStrideConfig(db, function(error, strideConfig) {
					if (error) {
						log.error(db, "connector.splitEvent getStrideConfig", error);
						return;
					}

					sendStride(db, text, detail1, detail2, proxyConfig);
				});
				break;
			case C.KEY_CONFIG_TWILIO_ACTIVE:
				configurator.getTwilioConfig(db, function(error, twilioConfig) {
					if (error) {
						log.error(db, "connector.splitEvent getTwilioConfig", error);
						return;
					}
					var accountSid = twilioConfig[C.KEY_CONFIG_TWILIO_SID];
					var authToken = twilioConfig[C.KEY_CONFIG_TWILIO_AUTHTOKEN];

					sendTwilio(db, text, detail1, accountSid, authToken, proxyConfig);
				});
				break;
			case C.KEY_CONFIG_ANDROID_ACTIVE:
				configurator.getAndroidConfig(db, function(error, androidConfig) {
					if (error) {
						log.error(db, "connector.splitEvent getAndroidConfig", error);
						return;
					}
					var fb_project_id = androidConfig[C.KEY_CONFIG_ANDROID_PROJECTID];
					var fb_private_key_id = androidConfig[C.KEY_CONFIG_ANDROID_PRIVATEKEYID];
					var fb_private_key = androidConfig[C.KEY_CONFIG_ANDROID_PRIVATEKEY];
					var fb_client_email = androidConfig[C.KEY_CONFIG_ANDROID_EMAIL];
					var fb_client_id = androidConfig[C.KEY_CONFIG_ANDROID_CLIENTID];
					var fb_client_x509_cert_url = androidConfig[C.KEY_CONFIG_ANDROID_CERTURL];
					
					sendAndroidPush(db, detail1, text, fb_project_id, fb_private_key_id, fb_private_key, fb_client_email, fb_client_id, fb_client_x509_cert_url, proxyConfig);
				});
				break;
			case C.KEY_CONFIG_AUDIT_ACTIVE:
				sendAuditLog(db, text, detail1);
				break;
			case C.KEY_CONFIG_REST_ACTIVE:
				configurator.getRESTConfig(db, function(error, restConfig) {
					if (error) {
						log.error(db, "connector.splitEvent getRESTConfig", error);
						return;
					}

					sendREST(db, text, detail1, detail2, proxyConfig);
				});
				break;
			case C.KEY_CONFIG_HANGOUT_ACTIVE:
				configurator.getHangoutConfig(db, function(error, hangoutConfig) {
					if (error) {
						log.error(db, "connector.splitEvent getHangoutConfig", error);
						return;
					}

					sendHangoutChat(db, text, detail1, proxyConfig);
				});
				break;
		}
	});
}

function _getValueFromConfigurationArray(key, array) {
	for (var i=0; i<array.length; i++) {
		if (key === array[i].key) {
			return array[i].value;
		}
	}
	
	return "";
}

module.exports = {
	
	/*
	Sends out one notification for each notification specified in the trigger object
	*/
	sendEvent: function(db, event, trigger) {
		for (var i=0; i<trigger.notifications.length; i++) {
			var text = trigger.notifications[i].mtext.toString();
			var detail1 = trigger.notifications[i].mdetail1.toString();
			var detail2 = trigger.notifications[i].mdetail2.toString();
			var detail3 = trigger.notifications[i].mdetail3.toString();
			var medium = trigger.notifications[i].mmedium;
			
			splitEvent(db, text, detail1, detail2, detail3, medium);
		}
	},
	
	/*
	Sends out one notification for each notification specified in the trigger object. Adds an additional information ("count") to the text
	*/
	sendBundledEvents: function(db, trigger, count) {
		for (var i=0; i<trigger.notifications.length; i++) {
			var text = trigger.notifications[i].mtext.toString() + " (" + count + ")";
			var detail1 = trigger.notifications[i].mdetail1.toString();
			var detail2 = trigger.notifications[i].mdetail2.toString();
			var detail3 = trigger.notifications[i].mdetail3.toString();
			var medium = trigger.notifications[i].mmedium;
			
			splitEvent(db, text, detail1, detail2, detail3, medium);
		}
	},
	test: function(db, medium, configuration, text, detail1, detail2, callback) {
		configurator.getProxyConfig(db, function(error, proxyConfig) {
			log.debug("sending test message to " + medium);
			switch (medium) {
				case C.KEY_CONFIG_MAIL_ACTIVE:
					var host = _getValueFromConfigurationArray(C.KEY_CONFIG_MAIL_HOST, configuration);
					var port =  _getValueFromConfigurationArray(C.KEY_CONFIG_MAIL_PORT, configuration);
					var secure =  _getValueFromConfigurationArray(C.KEY_CONFIG_MAIL_TLS, configuration);
					var rejectUnauthorized =  _getValueFromConfigurationArray(C.KEY_CONFIG_MAIL_REJECTUNAUTH, configuration);
					var from =  _getValueFromConfigurationArray(C.KEY_CONFIG_MAIL_FROM, configuration);
					var user =  _getValueFromConfigurationArray(C.KEY_CONFIG_MAIL_USERNAME, configuration);
					var password =  _getValueFromConfigurationArray(C.KEY_CONFIG_MAIL_PASSWORD, configuration);

					sendMail(db, text, detail1, detail2, host, port, secure, rejectUnauthorized, from, user, password, proxyConfig, callback);
					break;
				case C.KEY_CONFIG_SLACK_ACTIVE:
					var webhookUrl = _getValueFromConfigurationArray(C.KEY_CONFIG_SLACK_HOOK, configuration);
					sendSlack(db, text, detail1, webhookUrl, proxyConfig, callback);
					break;
				case C.KEY_CONFIG_STRIDE_ACTIVE:
					sendStride(db, text, detail1, detail2, proxyConfig, callback);
					break;
				case C.KEY_CONFIG_TWILIO_ACTIVE:
					var accountSid = _getValueFromConfigurationArray(C.KEY_CONFIG_TWILIO_SID, configuration);
					var authToken =  _getValueFromConfigurationArray(C.KEY_CONFIG_TWILIO_AUTHTOKEN, configuration);
					sendTwilio(db, text, detail1, accountSid, authToken, proxyConfig, callback);
					break;
				case C.KEY_CONFIG_ANDROID_ACTIVE:
					var fb_project_id = _getValueFromConfigurationArray(C.KEY_CONFIG_ANDROID_PROJECTID, configuration);
					var fb_private_key_id =  _getValueFromConfigurationArray(C.KEY_CONFIG_ANDROID_PRIVATEKEYID, configuration);
					var fb_private_key = _getValueFromConfigurationArray(C.KEY_CONFIG_ANDROID_PRIVATEKEY, configuration);
					var fb_client_email =  _getValueFromConfigurationArray(C.KEY_CONFIG_ANDROID_EMAIL, configuration);
					var fb_client_id = _getValueFromConfigurationArray(C.KEY_CONFIG_ANDROID_CLIENTID, configuration);
					var fb_client_x509_cert_url =  _getValueFromConfigurationArray(C.KEY_CONFIG_ANDROID_CERTURL, configuration);
					
					sendAndroidPush(db, detail1, text, fb_project_id, fb_private_key_id, fb_private_key, fb_client_email, fb_client_id, fb_client_x509_cert_url, proxyConfig, callback);
					break;
				case C.KEY_CONFIG_AUDIT_ACTIVE:
					sendAuditLog(db, text, detail1, callback);
					break;
				case C.KEY_CONFIG_REST_ACTIVE:
					sendREST(db, text, detail1, detail2, proxyConfig, callback);
					break;
				case C.KEY_CONFIG_HANGOUT_ACTIVE:
					sendHangoutChat(db, text, detail1, proxyConfig, callback);
					break;
			} 
		});
	}
	
};

