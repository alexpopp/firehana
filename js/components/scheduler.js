/*eslint no-console: 0, no-unused-vars: 0*/
"use strict";
var jobsc = require("@sap/jobs-client");
var xsenv = require("@sap/xsenv");
var dbaccess = require("../utils/dbaccess");
var log = require("../utils/log");
var C = require("../utils/constants");


var EVENT_SCHEDULER_KEY = "firehanaEventScheduler"; //jobId = mvalue1, cron = mvalue2
var EVENT_SCHEDULER_NAME = "firehanaEventScheduler";
var EVENT_SCHEDULER_DESCRIPTION = "This is the FireHANA scheduler for detecting new events. Do not delete this job manually.";

var NOTIFICATION_SCHEDULER_KEY = "firehanaNotificationScheduler"; //jobId = mvalue1
var NOTIFICATION_SCHEDULER_NAME = "firehanaNotificationScheduler";
var NOTIFICATION_SCHEDULER_DESCRIPTION = "This is the FireHANA scheduler for sending out notifications. Do not delete this job manually.";
var NOTIFICATION_SCHEDULER_DESCRIPTION_HOUR = "This is the FireHANA scheduler for sending out notifications every hour. Do not delete this job manually.";
var NOTIFICATION_SCHEDULER_DESCRIPTION_DAY = "This is the FireHANA scheduler for sending out notifications every day. Do not delete this job manually.";
var NOTIFICATION_SCHEDULER_DESCRIPTION_WEEK = "This is the FireHANA scheduler for sending out notifications every week. Do not delete this job manually.";
var CRON_HOURLY = "* * * * * 0 0"; //every hour
var CRON_DAILY = "* * * * 0 0 0"; //every day at midnight
var CRON_WEEKLY = "* * * mon 0 0 0"; //every week monday midnight


/*
	Returns undefined or the currently active scheduler with key "key". This only looks in the local Config table and not if the scheduler is actually running.
*/
function getActiveScheduler(db, key, callback) {
	dbaccess.getConfig(db, key, function(error, mvalue1, mvalue2) {
		//TODO: get currently running job (if in db but not running -> delete from db; if not in db but running -> stop job)
		callback(error, mvalue1, mvalue2);
	});      
}

function stopScheduler(db, key, callback) {
	getActiveScheduler(db, key, function(error, mvalue1, mvalue2) {
			if (error) {
				log.error(db, "scheduler.stopScheduler getActiveScheduler", error);
				callback(error, undefined);
			} else if (mvalue1) {
				var services = xsenv.getServices({jobscheduler:{ tag: "jobscheduler" }}).jobscheduler;
				var options = {
					timeout: 15000,
					user: services.user,
					password: services.password,
					baseURL: services.url};	
					
				var myJob = {
					"jobId": mvalue1
				};
				var scheduler = new jobsc.Scheduler(options);
				scheduler.deleteJob(myJob, function(error2, body) {
					if (error2) {
						log.error(db, "scheduler.stopScheduler deleteJob", error2);
					}
					
					dbaccess.deleteConfig(db, key, callback);
				});	
			} else {
				callback(undefined);	
			}
		});
}

function startScheduler(db, key, name, description, appURL, schedules, callback) {
	stopScheduler(db, key, function() {
		var services = xsenv.getServices({jobscheduler:{ tag: "jobscheduler" }}).jobscheduler;	
		var startTime = new Date();
		startTime.setDate(startTime.getDate() - 1);
		var endTime = new Date();
		endTime.setDate(endTime.getDate() + 9999);
		
		var options = {
			timeout: 15000,
			user: services.user,
			password: services.password,
			baseURL: services.url};
		
		
		var finalSchedules = [];
		for (var i=0; i<schedules.length; i++) {
			finalSchedules.push({
					"cron": schedules[i].cron,
					"description": schedules[i].description,
					"data": {
						"jobname": name,
						"frequency": schedules[i].frequency
					},
					"active": true,
					"startTime": {
						"date": startTime.toISOString(),
						"format": "YYYY-MM-DDTHH:mm:ss.sssZ"
					},
					"endTime": {
						"date": endTime.toISOString(),
						"format": "YYYY-MM-DDTHH:mm:ss.sssZ"
					}
				});	
		}
		var myJob = {
			"name": name,
			"description": description,
			"action": appURL,
			"active": false,
			"httpMethod": "GET",
			"schedules": finalSchedules
		};
		
		
		var scheduler = new jobsc.Scheduler(options);
		var scJob = {
			job: myJob
		};
		
		var jobid;
		var scheduleid;
		scheduler.createJob(scJob, function(error, body) {
			if (error) {
				log.error(db, "scheduler.startNotificationJobScheduler createJob", error);
				callback(error, undefined);
			} else {
				jobid = body._id;
				scheduleid = body.schedules[0].scheduleId;
				var upJob = {
					"jobId": jobid,
					"job": {
						"active": true,
						"user": services.user,
						"password": services.password
					}
				};
				
				scheduler.updateJob(upJob, function(error2, body2) {
					if (error2) {
						log.error(db, "scheduler.startNotificationJobScheduler updateJob", error);
						callback(error2, undefined);
					} else {
						callback(undefined, jobid, scheduleid);
					}
	
				});
				
				
			}
		});		
	});
}

/*
	getActive, stop and start Methods for both schedulers: Event and Notification.
		-> Event scheduler looks at the Event table if there are new events and sends the to the validator
		-> Event scheduler looks for bundled events and sends them out at the appropriate interval (currently HOUR, DAY, WEEK)
*/
module.exports = {
	getActiveEventScheduler: function(db, callback) {
		getActiveScheduler(db, EVENT_SCHEDULER_KEY, function(error, mvalue1, mvalue2) {
			if (error) {
				log.error(db, "scheduler.getActiveEventScheduler getActiveScheduler", error);
				callback(error, undefined);
			}
			callback(undefined, {"mkey": mvalue1, "cron": mvalue2});		
		});
	},
	getActiveNotificationScheduler: function(db, callback) {
		getActiveScheduler(db, NOTIFICATION_SCHEDULER_KEY, function(error, mvalue1, mvalue2) {
			if (error) {
				log.error(db, "scheduler.getActiveNotificationScheduler getActiveScheduler", error);
				callback(error, undefined);
			}
			callback(undefined, {"mkey": mvalue1});		
		});
	},
	stopEventScheduler: function(db, callback) {
		stopScheduler(db, EVENT_SCHEDULER_KEY, function(error) {
			if (error) {
				log.error(db, "scheduler.stopEventScheduler stopScheduler", error);
			}	
			callback(error);
		});
	},
	stopNotificationScheduler: function(db, callback) {
		stopScheduler(db, NOTIFICATION_SCHEDULER_KEY, function(error) {
			if (error) {
				log.error(db, "scheduler.stopNotificationScheduler stopScheduler", error);
			}	
			callback(error);
		});
	},
	startEventScheduler: function(db, cronKey, url, callback) {
		var callbackUrl = url + "/scheduler/eventcallback";
		var cron = "* * * * * 0 0"; //default: every hour
		switch (cronKey) {
			case "ONE_SECOND":
				cron = "* * * * * * *"; //every second
				break;
			case "FIVE_SECONDS":
				cron = "* * * * * * */5"; //every 5 seconds
				break;
			case "ONE_MINUTE":
				cron = "* * * * * * 0"; //every minute
				break;
			case "TWO_MINUTES":
				cron = "* * * * * */2 0"; //every 2 minutes
				break;
			case "ONE_HOUR":
				cron = "* * * * * 0 0"; //every hour
				break;
		}
		var schedules = [{"cron": cron, "description": EVENT_SCHEDULER_DESCRIPTION, "frequency": "unused"}];
		
		startScheduler(db, EVENT_SCHEDULER_KEY, EVENT_SCHEDULER_NAME, EVENT_SCHEDULER_DESCRIPTION, callbackUrl, schedules, function(error, jobid, scheduleid) {
			if (error) {
				log.error(db, "scheduler.startEventScheduler startScheduler", error);
				callback(error);
			} else {	
				dbaccess.saveOrUpdateConfig(db, EVENT_SCHEDULER_KEY, jobid, cronKey, function(error2) {
					if (error) {
						log.error(db, "scheduler.startEventScheduler saveOrUpdateConfig", error2);
					}	
					callback(error);
				});             
			}
		});
	},
	startNotificationScheduler: function(db, url, callback) {
		var callbackUrl = url + "/scheduler/notificationcallback";
		var schedules = [
			{"cron": CRON_HOURLY, "description": NOTIFICATION_SCHEDULER_DESCRIPTION_HOUR, "frequency": "HOUR"},
			{"cron": CRON_DAILY, "description": NOTIFICATION_SCHEDULER_DESCRIPTION_DAY, "frequency": "DAY"},
			{"cron": CRON_WEEKLY, "description": NOTIFICATION_SCHEDULER_DESCRIPTION_WEEK, "frequency": "WEEK"}];
		
		startScheduler(db, NOTIFICATION_SCHEDULER_KEY, NOTIFICATION_SCHEDULER_NAME, NOTIFICATION_SCHEDULER_DESCRIPTION, callbackUrl, schedules, function(error, jobid, scheduleid) {
			if (error) {
				log.error(db, "scheduler.startNotificationScheduler startScheduler", error);
				callback(error);
			} else {	
				dbaccess.saveOrUpdateConfig(db, NOTIFICATION_SCHEDULER_KEY, jobid, scheduleid, function(error2) {
					if (error) {
						log.error(db, "scheduler.startNotificationScheduler saveOrUpdateConfig", error2);
					}	
					callback(error);
				});             
			}
		});
	}
	
};

