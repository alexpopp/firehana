/*eslint no-console: 0, no-unused-vars: 0*/
"use strict";
var dbaccess = require("../utils/dbaccess");
var triggerer = require("./triggerer");
var connector = require("./connector");
var async = require("async");
var log = require("../utils/log");
var C = require("../utils/constants");


/*
	Events are written by database triggers to the Event table with "mprocessed = 0".
	The Event Scheduler regularly calls "validateNEwEvents". This function looks for events with "mprocessed = 0".
		-> If an event has the timing "immediate", it will be sent to the connector and "mprocessed = 2"
		-> If an event has the timing "bundled" then "mprocessed = 1"
	The Event Scheduler regularly calls "validateStagedEvents".  This function looks for events with "mprocessed = 1"
		-> If an event has the timing matched to the scheduler call (HOUR, DAY, WEEK) it will be sent to the connector and "mprocessed = 2"
*/
module.exports = {
	
	/*
		Looks for all events with "mprocessed = 0" and calls "validateNewEvent" for each one of that. Aditionally, counts all events for each trigger and incremenents the event count.
	*/
	validateNewEvents: function(db, callback) {
		var self = this;
		dbaccess.execute(db, "select * from \"" + C.TABLE_EVENT + "\" where \"mprocessed\" = 0", function(error, result) {
			
			if (error) {
				log.error(db, "validator.validateNewEvents execute", error);
				callback();
				return;
			}
			
			async.forEachOf(result, function(event, key, cb) {
			    self.validateNewEvent(db, event, cb);
			}, function(err){
			    if (err) {
			    	log.error(db, "validator.validateNewEvents forEachOf1", err);
			    }
			    
			    var eventCounts = [];
			    for (var i=0; i<result.length; i++) {
			    	var alreadyAdded = false;
			    	for (var j=0; j<eventCounts.length; j++) {
	    				if (eventCounts[j].mkey === result[i].mtriggerkey) {
			    			alreadyAdded = true;
			    			eventCounts[j].count = eventCounts[j].count + 1;
			    			break;
			    		}	
			    	}
			    	
			    	if (!alreadyAdded) {
			    		eventCounts.push({mkey: result[i].mtriggerkey, count: 1});	
			    	}
		    	}
		    	
		    	async.forEachOf(eventCounts, function(eventCount, key, cb) {
		    		triggerer.getTriggerObject(db, eventCount.mkey, function(error2, triggerObject) {
		    			if (error2) {
					    	log.error(db, "validator.validateNewEvents getTriggerObject", error2);
					    }	
    					var newEventCount = triggerObject.meventcount + eventCount.count;
						dbaccess.execute(db, "update \"" + C.TABLE_TRIGGER + "\" set \"meventcount\" = " + newEventCount + " where \"mkey\" = " + triggerObject.mkey + "", function(error1, result1) {
								if (error1) {
									log.error(db, "validator.validateNewEvent execute1", error1);
								}
						});
		    		});
				}, function(err2) {
					if (err2) {
						log.error(db, "validator.validateNewEvents forEachOf2", err2);	
					}
					callback();
				});
			});
		});
	},
	
	/*
		Looks for all events with "mprocessed = 1" and calls "validateStagedEvent" for each of these. 
		This functions return the trigger object if it has a matching frequency for this "validateStagedEvents" call (HOUR, DAY, WEEK). These trigger objects are saved in "allDueTriggers"
		After all individual events are processed, "allDueTriggers" will be transformed so that there is an object "{key, count, triggerObject}" for each different trigger. 
		"Count" is calculated by the number of events in "allDueTriggers" for this trigger.
		For each trigger, this object will be sent to the connector.
	*/
	validateStagedEvents: function(db, frequency, callback) {
		var self = this;
		dbaccess.execute(db, "select * from \"" + C.TABLE_EVENT + "\" where \"mprocessed\" = 1", function(error, result) {
			
			if (error) {
				log.error(db, "validator.validateStagedEvents execute", error);
				callback();
				return;
			}
			
			var allDueTriggers = [];
			async.forEachOf(result, function(event, key, cb) {
			    self.validateStagedEvent(db, event, frequency, function(trigger) {
			    	if (trigger) {
			    		allDueTriggers.push(trigger);		
			    	}
			    });
			}, function(err){
				if (err) {
					log.error(db, "validator.validateStagedEvent forEachOf", err);
				}
				
				var bundles = []; //{key: trigger.mkey, count: INT, trigger: triggerObject}
				for (var i=0; i<allDueTriggers.length; i++) {
					for (var j=0; j<bundles.length; j++) {
						var alreadyAdded = false;
						if (allDueTriggers[i].trigger.mkey === bundles[j].key) {
							alreadyAdded = true;	
							bundles[j].count = bundles[j].count + 1;
							break;
						} 
						
						if (!alreadyAdded) {
							bundles.push({key: allDueTriggers[i].trigger.mkey, count: 1, trigger: allDueTriggers[i]});	
						}
					}
				}
				
				for (var k=0; k<bundles.length; k++) {
					connector.sendBundledEvents(db, bundles[i].trigger, bundles[i].count);
				}
			    callback();
			});
		});
	},

	/*
		If the event has the timing "immediate" it status (mprocessed) wil be set to 2 and the event sent to the connector, otherwise the status will be set to 1
	*/
	validateNewEvent: function(db, event, callback) {
		triggerer.getTrigger(db, event.mtriggerkey, function(error, trigger) {
					if (error) {
						log.error(db, "validator.validateNewEvent getTrigger", error);
						callback();
					} else {
							if (trigger.timing.mimmediate) {
								//if immediate send it out
								dbaccess.execute(db, "update \"" + C.TABLE_EVENT + "\" set \"mprocessed\" = 2 where \"mkey\" = " + event.mkey + "", function(error2, result2) {
									if (error2) {
										log.error(db, "validator.validateNewEvent execute2", error2);
									}
									connector.sendEvent(db, event, trigger);
									callback();
								});
							} else  {
								//not immediate -> set in bundle state
								dbaccess.execute(db, "update \"" + C.TABLE_EVENT + "\" set \"mprocessed\" = 1 where \"mkey\" = " + event.mkey + "", function(error2, result2) {
									if (error2) {
										log.error(db, "validator.validateNewEvent execute3", error2);
									}
									callback();
								});
							} 
					}
				});
	},
	
	/*
		If an event has the same frequency as "frequency" (HOUR, DAY, WEEK), its staus (mprocessed) will be set to 2 and the trigger returned (REMARK: not directly send to the connector as in "validateNewEvent")
	*/
	validateStagedEvent: function(db, event, frequency, callback) {
		triggerer.getTrigger(db, event.mtriggerkey, function(error, trigger) {
					if (error) {
						log.error(db, "validator.validateStagedEvent getTrigger", error);
						callback();
					} else {
						if (frequency === trigger.timing.mtime) {
							dbaccess.execute(db, "update \"" + C.TABLE_EVENT + "\" set \"mprocessed\" = 2 where \"mkey\" = " + event.mkey + "", function(error2, result2) {
								if (error2) {
									log.error(db, "validator.validateStagedEvent execute", error2);
								}
								
								callback(trigger);
							});
						} else {
							callback();
						}
					}
				});
	}

};

