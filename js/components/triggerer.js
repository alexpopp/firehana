/*eslint no-console: 0, no-unused-vars: 0*/
"use strict";
var dbaccess = require("../utils/dbaccess");
var C = require("../utils/constants");
var async = require("async");
var log = require("../utils/log");


module.exports = {
	
	/*
	Return all tables which can be selected by the user in the "activation" UI
	*/
	getAvailableTables: function(db, callback) {
		dbaccess.execute(db, "select SCHEMA_NAME, TABLE_NAME from public.tables", function(error, result) { //TODO get only tables with TRIGGER privilege
			if (error) {
				log.error(db, "triggerer.getAvailableTables execute", error);
			}
			callback(error, result);
		});
	},
	
	/*
	Return all columns for the previously selected table in the "filter" UI
	*/
	getAvialableColumns: function(db, schema, table, callback) {
		dbaccess.execute(db, "select \"COLUMN_NAME\", \"DATA_TYPE_NAME\", \"LENGTH\" from \"PUBLIC\".\"TABLE_COLUMNS\" where \"SCHEMA_NAME\" = '" + schema + "' AND \"TABLE_NAME\" = '" + table + "' order by \"POSITION\"", function(error, result) {
			if (error) {
				log.error(db, "triggerer.getAvialableColumns execute", error);
			}
			callback(error, result);
		});	
	},
	
	/*
	Return all available mediums, the ones that are set to active in the administration page. Only if activated, a notification for this medium can be created.
	*/
	getAvailableMediums: function(db, callback) {
		var mediumConfigAtiveKeys = [C.KEY_CONFIG_MAIL_ACTIVE, C.KEY_CONFIG_SLACK_ACTIVE, C.KEY_CONFIG_STRIDE_ACTIVE, C.KEY_CONFIG_TWILIO_ACTIVE, C.KEY_CONFIG_AUDIT_ACTIVE, C.KEY_CONFIG_ANDROID_ACTIVE, C.KEY_CONFIG_REST_ACTIVE, C.KEY_CONFIG_HANGOUT_ACTIVE];
		var availableMediums = [];
		async.forEachOf(mediumConfigAtiveKeys, function(mediumActiveKey, key, cb) {
			    dbaccess.getConfig(db, mediumActiveKey, function(error, config) {
			    	if (error) {
			    		log.error(db, "triggerer.getAvailableMediums getConfig", error);
			    	}
			    	if (config) {
			    		availableMediums.push(mediumActiveKey);
			    	}
			    	
			    	cb();
			    });
			}, function(err){
			    callback(err, availableMediums);
			});	
	},
	
	/*
		Returns all trigger objects. But only the "head object". Corresponding activations, filters, notifications and timings are not loaded.
	*/
	getAllTriggers: function(db, callback) {
		dbaccess.execute(db, "select * from \"" + C.TABLE_TRIGGER + "\"", function(error, result) {
			if (error) {
				log.error(db, "triggerer.getAllTriggers execute", error);
			}
			callback(error, result);
		});
	},
	
	/*
		Returns all the data (trigger, activation, filters, notifications, timing) for a trigger
	*/
	getTrigger: function(db, id, callback) {
		var self = this;
		dbaccess.execute(db, "select * from \"" + C.TABLE_TRIGGER + "\" where \"mkey\" = '" + id + "'", function(error, result) {
			if (error) {
				log.error(db, "triggerer.getTrigger execute", error);
				callback(error, undefined);
			} else {
				self._getAllTriggerData(db, result[0], callback);
			}
		});
	},
	
	/*
		Returns only the "head object" for a trigger. Corresponding activations, filters, notifications and timings are not loaded.
	*/
	getTriggerObject: function(db, id, callback) {
		var self = this;
		dbaccess.execute(db, "select * from \"" + C.TABLE_TRIGGER + "\" where \"mkey\" = '" + id + "'", function(error, result) {
			if (error) {
				log.error(db, "triggerer.getTriggerObject execute", error);
				callback(error, undefined);
			} else {
				callback(undefined, result[0]);
			}
		});
	},
	
	/*
	Deletes a trigger and all objects (trigger, activation, filters, notifications, timing) that reference to that trigger.
	*/
	deleteTrigger: function(db, triggerId, callback) {
		this.deactivateTrigger(db, triggerId, function(error0, result0) {
			async.parallel([
			    function(cb) {
					dbaccess.execute(db, "delete from \"" + C.TABLE_ACTIVATION + "\" where \"mtrigger.mtriggerkey\" = " + triggerId, function(error, result) {
							if (error) {
								log.error(db, "triggerer.deleteTrigger execute1", error);
							} 
							cb(error);
						});	
			    },
			    function(cb) {
					dbaccess.execute(db, "delete from \"" + C.TABLE_TIMING + "\" where \"mtrigger.mtriggerkey\" = " + triggerId, function(error, result) {
						if (error) {
							log.error(db, "triggerer.deleteTrigger execute2", error);
						} 
						cb(error);
					});
			    },
			    function(cb) {
					dbaccess.execute(db, "delete from \"" + C.TABLE_FILTER + "\" where \"mtrigger.mtriggerkey\" = " + triggerId, function(error, result) {
						if (error) {
							log.error(db, "triggerer.deleteTrigger execute3", error);
						} 
						cb(error);
					});
			    },
			    function(cb) {
					dbaccess.execute(db, "delete from \"" + C.TABLE_NOTIFICATION + "\" where \"mtrigger.mtriggerkey\" = " + triggerId, function(error, result) {
						if (error) {
							log.error(db, "triggerer.deleteTrigger execute4", error);
						} 
						cb(error);
					});	
			    }
			], function(err, results) {
				if (!err) { //do not delete trigger object if there was an error
					dbaccess.execute(db, "delete from \"" + C.TABLE_TRIGGER + "\" where \"mkey\" = " + triggerId, function(error, result) {
						if (error) {
							log.error(db, "triggerer.deleteTrigger execute5", error);
						}
						callback(error);
					});
				} else {
					callback(err);	
				}
			});
		});
	},
	
	/*
		Creates a new trigger object.
	*/
	createTrigger: function(db, triggerName, callback) {
		var self = this;
		dbaccess.execute(db, "SELECT SESSION_CONTEXT('APPLICATIONUSER') \"APPLICATION_USER\" FROM \"DUMMY\"", function(error, result) {
			var applicationuser = "unknown";
			if (error) {
				log.error(db, "triggerer.createTrigger execute1", error);
			}
			if (!error && result[0] && result[0].APPLICATION_USER) {
				applicationuser = result[0].APPLICATION_USER;
			} 
		
			triggerName = triggerName.toUpperCase();	
			dbaccess.execute(db, "insert into \"" + C.TABLE_TRIGGER + "\" values(\"triggerSeqKey\".NEXTVAL, '"+ triggerName + "', 0, 0, CURRENT_TIMESTAMP, '" + applicationuser + "', FALSE)", function(error2, result2) {
				if (error2) {
					log.error(db, "triggerer.createTrigger execute2", error2);
					callback(error2, result2);	
				} else {
					self._getTriggerByName(db, triggerName, callback);
				}
			});
		});
	},
	
	/*
		Activates a trigger. This actually creates a database trigger.
	*/
	activateTrigger: function(db, triggerId, callback) {
		this.getTrigger(db, triggerId, function(error, result) {
			
			if (!result.activation || !result.timing || !result.notifications) {
				callback("You need to have an activation, timing and notification to activate the trigger", {});
				return;
			}

			if (error) {
				log.error(db, "triggerer.activateTrigger getTrigger", error);
				callback(error, result);	
			} else {
				var triggerName = result.trigger.mname;
				var operation = result.activation.moperation.toLowerCase();
				var tableName = "\"" + result.activation.mschema + "\".\"" + result.activation.mtable + "\"";
				var conditionsStart = "";
				for (var i=0; i<result.filters.length; i++) {
					var filter = i > 0 ? " and " : "";
					var comparator = "=";
					switch (result.filters[i].mcomparator) {
						case "BIGGER":
							comparator = ">"; break;
						case "BIGGER_OR_EQUAL":
							comparator = ">="; break;
						case "SMALLER":
							comparator = "<"; break;
						case "SMALLER_OR_EQUAL":
							comparator = "<="; break;
					}
					
					var value = result.filters[i].mdatatype === "BOOLEAN" ? result.filters[i].mvalue : "'" + result.filters[i].mvalue + "'";
					filter = filter + ":mynewrow.\"" + result.filters[i].mcolumn + "\" " + comparator + " " + value;
					conditionsStart += filter;	
				}
				conditionsStart = result.filters.length > 0 ? "if " + conditionsStart + " then" : "";
				var oldOrNew = operation === "delete" ? "old" : "new";
				var conditionsEnd = result.filters.length > 0 ? "end if;" : "";
								dbaccess.execute(db, " SELECT CURRENT_SCHEMA FROM DUMMY;", 
									function(error2, result2) {
										if (error2 || !result2[0].CURRENT_SCHEMA) {
											log.error(db, "triggerer.activateTrigger execute1", error2);
											callback(error2, result2);
										} else {
											dbaccess.execute(db, "create trigger " + triggerName + 
												" after " + operation + 
												" on "+ tableName + " referencing " + oldOrNew + " row mynewrow for each row" +
												" begin " + 
												conditionsStart + 
												" insert into \"" + result2[0].CURRENT_SCHEMA + "\".\"" + C.TABLE_EVENT + "\" values (\"" + result2[0].CURRENT_SCHEMA + "\".\"eventSeqKey\".NEXTVAL, current_user, now(), 0, " + triggerId + "); " +
												conditionsEnd + 
												" end;", 
											function(error3, result3) {
												if (error3) {
													log.error(db, "triggerer.activateTrigger execute2", error3);
													callback(error3, result3);
												} else {
													dbaccess.execute(db, "update \"" + C.TABLE_TRIGGER + "\" set \"mactivated\" = true, \"mstatus\" = 1 where \"mkey\" = " + triggerId, function(error4, result4) {
														if (error4) {
															log.error(db, "triggerer.activateTrigger execute3", error4);
														}
														callback(error4, result4);
													});	
												}
											});	
										}
									});	
			}
		});
	},
	
	/*
	Deletes the database trigger.
	*/
	deactivateTrigger: function(db, triggerId, callback) {
		this.getTrigger(db, triggerId, function(error, result) {
				if (error) {
					log.error(db, "triggerer.deactivateTrigger getTrigger", error);
					callback(error, undefined);
				} else if (result.trigger.mactivated) {
					dbaccess.execute(db, "drop trigger \"" + result.activation.mschema + "\".\"" + result.trigger.mname + "\"", function(error2, result2) {
						if (error2) {
							log.error(db, "triggerer.deactivateTrigger execute1", error2);
						}
						dbaccess.execute(db, "update \"" + C.TABLE_TRIGGER + "\" set \"mactivated\" = false, \"mstatus\" = 0 where \"mkey\" = " + triggerId, function(error3, result3) {
								if (error3) {
									log.error(db, "triggerer.deactivateTrigger execute2", error3);
								}
								callback(error3, result3);
							});		
					});	
				} else {
					callback(undefined, undefined);
				}
			});	
	},
	
	/*
	Helper function to delete an object (activation, filters, notifications, timing) that references to a specified trigger.
	*/
	_deleteTriggerObject: function(db, objectId, table, callback) {
		var self = this;
		dbaccess.execute(db, "select * from \"" + table + "\" where \"mkey\" = '" + objectId + "'", function(error, result) {
			if (error) {
				log.error(db, "triggerer._deleteTriggerObject execute1", error);
				callback(error);
			} else if (!result[0]) {
				callback("Could not find object woth id " + objectId);	
			} else {
				var triggerId = result[0]["mtrigger.mtriggerkey"];	
				
				self.deactivateTrigger(db, triggerId, function(error1, result1) {
					dbaccess.execute(db, "delete from \"" + table + "\" where \"mkey\" = " + objectId, function(error2, result2) {
						if (error2) {
							log.error(db, "triggerer._deleteTriggerObject execute2", error2);
						}
						callback(error2);
					});
				});
			}
		});
	},
	createActivation: function(db, triggerId, schema, table, operation, callback) { 
		dbaccess.execute(db, "insert into \"" + C.TABLE_ACTIVATION + "\" values(\"activationSeqKey\".NEXTVAL, '"+ schema + "', '" + table + "', '" + operation + "', "+ triggerId + ")", function(error, result) {
			if (error) {
				log.error(db, "triggerer.createActivation execute", error);
			}
			callback(error);
		});
	},
	updateActivation: function(db, activationId, schema, table, operation, callback) { 
		dbaccess.execute(db, "update \"" + C.TABLE_ACTIVATION + "\" set \"mschema\" = '" + schema + "', \"mtable\" = '" + table + "', \"moperation\" = '" + operation + "' where \"mkey\" = " + activationId, function(error, result) {
			if (error) {
				log.error(db, "triggerer.updateActivation execute", error);
			}
			callback(error);
		});
	},
	deleteActivation: function(db, activationId, callback) { 
		this._deleteTriggerObject(db, activationId, C.TABLE_ACTIVATION, callback);
	},
	createTiming: function(db, triggerId, immediate, time, bundled, callback) { 
		dbaccess.execute(db, "insert into \"" + C.TABLE_TIMING + "\" values(\"timingSeqKey\".NEXTVAL, "+ immediate + ", '" + time + "', " + bundled + ", "+ triggerId + ")", function(error, result) {
			if (error) {
				log.error(db, "triggerer.createTiming execute", error);
			}
			callback(error);
		});
	},
	updateTiming: function(db, timingId, immediate, time, bundled, callback) { 
		dbaccess.execute(db, "update \"" + C.TABLE_TIMING + "\" set \"mimmediate\" = " + immediate + ", \"mtime\" = '" + time + "', \"mbundled\" = " + bundled + " where \"mkey\" = " + timingId, function(error, result) {
			if (error) {
				log.error(db, "triggerer.updateTiming execute", error);
			}
			callback(error);
		});
	},
	deleteTiming: function(db, timingId, callback) { 
		this._deleteTriggerObject(db, timingId, C.TABLE_TIMING, callback);
	},
	createFilter: function(db, triggerId, column, comparator, value, dataType, callback) { 
		dbaccess.execute(db, "insert into \"" + C.TABLE_FILTER + "\" values(\"filterSeqKey\".NEXTVAL, '"+  column + "', '" + comparator + "', '" + value + "', '" + dataType + "', " + triggerId + ")", function(error, result) {
			if (error) {
				log.error(db, "triggerer.createFilter execute", error);
			}
			callback(error);
		});
	},
	updateFilter: function(db, filterId, column, comparator, value, dataType, callback) { 
		dbaccess.execute(db, "update \"" + C.TABLE_FILTER + "\" set \"mcolumn\" = '" + column + "', \"mcomparator\" = '" + comparator + "', \"mvalue\" = '" + value + "', \"mdatatype\" = '" + dataType + "' where \"mkey\" = " + filterId, function(error, result) {
			if (error) {
				log.error(db, "triggerer.updateFilter execute", error);
			}
			callback(error);
		});
	},
	deleteFilter: function(db, filterId, callback) { 
		this._deleteTriggerObject(db, filterId, C.TABLE_FILTER, callback);
	},
	createNotification: function(db, triggerId, medium, text, detail1, detail2, detail3, callback) { 
		dbaccess.execute(db, "insert into \"" + C.TABLE_NOTIFICATION + "\" values(\"notificationSeqKey\".NEXTVAL, '"+  medium + "', "+ triggerId + ", '" + text + "', '" + detail1 + "', '" + detail2 + "', '" + detail3 + "')", function(error, result) {
			if (error) {
				log.error(db, "triggerer.createNotification execute", error);
			}
			callback(error);
		});
	},
	updateNotification: function(db, notificationId, medium, text, detail1, detail2, detail3, callback) { 
		dbaccess.execute(db, "update \"" +  C.TABLE_NOTIFICATION + "\" set \"mmedium\" = '" + medium + "', \"mtext\" = '" + text + "', \"mdetail1\" = '" + detail1 + "', \"mdetail2\" = '" + detail2 + "', \"mdetail3\" = '" + detail3 + "' where \"mkey\" = " + notificationId, function(error, result) {
			if (error) {
				log.error(db, "triggerer.updateNotification execute", error);
			}
			callback(error);
		});
	},
	deleteNotification: function(db, notificationId, callback) { 
		this._deleteTriggerObject(db, notificationId,  C.TABLE_NOTIFICATION, callback);
	},
	_getTriggerByName: function(db, name, callback) {
		var self = this;
		dbaccess.execute(db, "select * from \"" +  C.TABLE_TRIGGER + "\" where \"mname\" = '" + name + "'", function(error, result) {
			if (error) {
				log.error(db, "triggerer._getTriggerByName execute", error);
				callback(error, undefined);
			} else {
				self._getAllTriggerData(db, result[0], callback);
			}
		});	
	},
	
	/*
		Returns all the objects referencing to "trigger" (activation, filters, notifications, timing).
	*/
	_getAllTriggerData: function(db, trigger, callback) { 
		if (!trigger.mkey) {
			log.error(db, "triggerer._getAllTriggerData !trigger.mkey", "invalid trigger object");
			callback("invalid trigger object", undefined);	
			return;
		}
		
		var resultObject = {"trigger": trigger, "activation": undefined, "timing": undefined, "filters": [], "notifications": []};

			
		async.parallel([
		    function(cb) {
    			//Get activation
				dbaccess.execute(db, "select * from \"" +  C.TABLE_ACTIVATION + "\" where \"mtrigger.mtriggerkey\" = '" + trigger.mkey + "'", function(error, result) {
					if (error) {
						log.error(db, "triggerer._getAllTriggerData execute1", error);
						cb(error, undefined);
					} else {
						resultObject.activation = result[0];
						cb(undefined, undefined);
					}
				});	
		    },
		    function(cb) {
				//Get timing
				dbaccess.execute(db, "select * from \"" +  C.TABLE_TIMING + "\" where \"mtrigger.mtriggerkey\" = '" + trigger.mkey + "'", function(error, result) {
					if (error) {
						log.error(db, "triggerer._getAllTriggerData execute2", error);
						cb(error, undefined);
					} else {
						resultObject.timing = result[0];
						if (resultObject.timing) {
							resultObject.timing.mimmediate = resultObject.timing.mimmediate === 1 ? true : false;
							resultObject.timing.mbundled = resultObject.timing.mbundled === 1 ? true : false;							
						}
						cb(undefined, undefined);
					}
				});	
		    },
		    function(cb) {
				//Get filters
				dbaccess.execute(db, "select * from \"" +  C.TABLE_FILTER + "\" where \"mtrigger.mtriggerkey\" = '" + trigger.mkey + "'", function(error, result) {
					if (error) {
						log.error(db, "triggerer._getAllTriggerData execute3", error);
						cb(error, undefined);
					} else {
						resultObject.filters = result;
						cb(undefined, undefined);
					}
				});	
		    },
		    function(cb) {
				//Get notifications
				dbaccess.execute(db, "select * from \"" +  C.TABLE_NOTIFICATION + "\" where \"mtrigger.mtriggerkey\" = '" + trigger.mkey + "'", function(error, result) {
					if (error) {
						log.error(db, "triggerer._getAllTriggerData execute4", error);
						cb(error, undefined);
					} else {
						for (var i=0;i<result.length; i++) {
							result[i].mtext = result[i].mtext.toString();
							result[i].mdetail1 = result[i].mdetail1.toString();
							result[i].mdetail2 = result[i].mdetail2.toString();
							result[i].mdetail3 = result[i].mdetail3.toString();
						}
						
						resultObject.notifications = result;
						cb(undefined, undefined);
					}
				});	
		    }
		], function(err, results) {
		    callback(err, resultObject);
		});
	}
};

