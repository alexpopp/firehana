/*eslint no-console: 0, no-unused-vars: 0, no-shadow: 0, new-cap: 0*/
"use strict";
var xssec = require("@sap/xssec");
var xsenv = require("@sap/xsenv");
var express = require("express");
var validator = require("../../components/validator");
var connector = require("../../components/connector");
var util = require("../../utils/util");
var log = require("../../utils/log");

module.exports = function() {
	var app = express.Router();
	
	var uaaService = xsenv.getServices({
		uaa: {
			tag: "xsuaa"
		}
	});

	app.get("/eventcallback", function(req, res) {
		log.debug("GET /eventcallback");
		var xsuaaCredentials = uaaService.uaa;
		if (!xsuaaCredentials) {
			log.error(req.db, "callback.postEventcallback xsuaaCredentials", "uaa service not found");
			res.status(401).json({
				message: "uaa service not found"
			});
			return;
		}
		var SCOPE = xsuaaCredentials.xsappname + ".JOBSCHEDULER";

		var accessToken;
		if (req.headers.authorization) {
			accessToken = req.headers.authorization.split(' ')[1];
		} else {
			log.error(req.db, "callback.postEventcallback req.headers.authorization", "Authorization header not found");
			res.status(401).json({
				message: "Authorization header not found"
			});
			return;
		}
		xssec.createSecurityContext(accessToken, xsuaaCredentials, function(error, securityContext) {
			if (error) {
				log.error(req.db, "callback.postEventcallback createSecurityContext", error);
				res.status(401).json({
					message: "Invalid access token"
				});
				return;
			} else  {
				validator.validateNewEvents(req.db, function() {
					util.sendBack(res, undefined, "success");
				});
			} 
		});
	});
	
	app.get("/notificationcallback", function(req, res) {
		log.debug("GET /notificationcallback");
		var xsuaaCredentials = uaaService.uaa;
		if (!xsuaaCredentials) {
			log.error(req.db, "callback.postNotificationcallback xsuaaCredentials", "uaa service not found");
			res.status(401).json({
				message: "uaa service not found"
			});
			return;
		}
		var SCOPE = xsuaaCredentials.xsappname + ".JOBSCHEDULER";

		var accessToken;
		if (req.headers.authorization) {
			accessToken = req.headers.authorization.split(' ')[1];
		} else {
			log.error(req.db, "callback.postNotificationcallback req.headers.authorization", "Authorization header not found");
			res.status(401).json({
				message: "Authorization header not found"
			});
			return;
		}
		xssec.createSecurityContext(accessToken, xsuaaCredentials, function(error, securityContext) {
			if (error) {
				log.error(req.db, "callback.postNotificationcallback createSecurityContext", error);
				res.status(401).json({
					message: "Invalid access token"
				});
				return;
			} else  {

				var frequency = req.query.frequency;
				validator.validateStagedEvents(req.db, frequency, function() {
					util.sendBack(res, undefined, "success");
				});
			} 
		});
	});

	
	return app;
};
