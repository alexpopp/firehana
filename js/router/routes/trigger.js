/*eslint no-console: 0, no-unused-vars: 0, no-shadow: 0, new-cap: 0*/
"use strict";
var express = require("express");
var triggerer = require("../../components/triggerer");
var util = require("../../utils/util");
var log = require("../../utils/log");

module.exports = function() {
	var app = express.Router();
	
	//UTIL
	app.get("/util/tables", function(req, res) {
		log.debug("GET /util/tables");
		triggerer.getAvailableTables(req.db, function(error, result) {
			util.sendBack(res, error, result);		
		});
	});
	app.get("/util/tables/:schema/:table", function(req, res) {
		log.debug("GET /util/tables/:schema/:table");
		var schema = req.params.schema;
		var table = req.params.table;
		triggerer.getAvialableColumns(req.db, schema, table, function(error, result) {
			util.sendBack(res, error, result);		
		});
	});
	app.get("/util/mediums", function(req, res) {
		log.debug("GET /util/mediums");
		triggerer.getAvailableMediums(req.db, function(error, result) {
			util.sendBack(res, error, result);		
		});
	});

	//TRIGGER
	app.get("/trigger", function(req, res) {
		log.debug("GET /trigger");
		triggerer.getAllTriggers(req.db, function(error, result) {
			util.sendBack(res, error, result);		
		});
	});
	
	app.get("/trigger/:id", function(req, res) {
		log.debug("GET /trigger/:id");
		var id = req.params.id;

		triggerer.getTrigger(req.db, id, function(error, result) {
			util.sendBack(res, error, result);		
		});
	});
	
	app.post("/trigger", function(req, res) {
		log.info(req.db, "POST /trigger");
		triggerer.createTrigger(req.db, req.body.name, function(error, result) {
			util.sendBack(res, error, result);	
		});
	});
	
	app.put("/trigger/:id", function(req, res) {
		log.info(req.db, "PUT /trigger/:id");
		if (req.body.active !== undefined) {
			if (req.body.active === "true") {
				triggerer.activateTrigger(req.db, req.params.id, function(error, result) {
					util.sendBack(res, error);	
				});
			} else {
				triggerer.deactivateTrigger(req.db, req.params.id, function(error, result) {
					util.sendBack(res, error);	
				});	
			}
		} else {
			util.sendBack(res, undefined, "");		
		}
	});
	
	app.delete("/trigger/:id", function(req, res) {
		log.info(req.db, "DELETE /trigger/:id");		
		triggerer.deleteTrigger(req.db, req.params.id, function(error) {
				util.sendBack(res, error);	
		});
	});
	
	//ACTIVATION
	app.post("/activation", function(req, res) {
		log.info(req.db, "POST /activation");
		triggerer.createActivation(req.db, req.body.triggerId, req.body.schema, req.body.table, req.body.operation, function(error, result) {
			util.sendBack(res, error, result);	
		});
	});
	app.put("/activation/:id", function(req, res) {
		log.info(req.db, "PUT /activation/:id");
		triggerer.updateActivation(req.db, req.params.id, req.body.schema, req.body.table, req.body.operation, function(error, result) {
			util.sendBack(res, error, result);	
		});
	});
	app.delete("/activation/:id", function(req, res) {
		log.info(req.db, "DELETE /activation/:id");
		triggerer.deleteActivation(req.db, req.params.id, function(error, result) {
			util.sendBack(res, error);	
		});
	});
	
	//TIMING
	app.post("/timing", function(req, res) {
		log.info(req.db, "POST /timing");
		triggerer.createTiming(req.db, req.body.triggerId, req.body.immediate, req.body.time, req.body.bundled, function(error, result) {
			util.sendBack(res, error, result);	
		});
	});
	app.put("/timing/:id", function(req, res) {
		log.info(req.db, "PUT /timing/:id");
		triggerer.updateTiming(req.db, req.params.id, req.body.immediate, req.body.time, req.body.bundled, function(error, result) {
			util.sendBack(res, error, result);	
		});
	});
	app.delete("/timing/:id", function(req, res) {
		log.info(req.db, "DELETE /timing/:id");
		triggerer.deleteTiming(req.db, req.params.id, function(error, result) {
			util.sendBack(res, error);	
		});
	});
	
	//FILTER
	app.post("/filter", function(req, res) {
		log.info(req.db, "POST /filter");
		triggerer.createFilter(req.db, req.body.triggerId, req.body.column, req.body.comparator, req.body.value, req.body.dataType, function(error, result) {
			util.sendBack(res, error, result);	
		});
	});
	app.put("/filter/:id", function(req, res) {
		log.info(req.db, "PUT /filter/:id");
		triggerer.updateFilter(req.db, req.params.id, req.body.column, req.body.comparator, req.body.value, req.body.dataType, function(error, result) {
			util.sendBack(res, error, result);	
		});
	});
	app.delete("/filter/:id", function(req, res) {
		log.info(req.db, "DELETE /filter/:id");
		triggerer.deleteFilter(req.db, req.params.id, function(error, result) {
			util.sendBack(res, error);	
		});
	});
	
	//NOTIFICARION
	app.post("/notification", function(req, res) {
		log.info(req.db, "POST /notification");
		triggerer.createNotification(req.db, req.body.triggerId, req.body.medium, req.body.text, req.body.detail1, req.body.detail2, req.body.detail3, function(error, result) {
			util.sendBack(res, error, result);	
		});
	});
	app.put("/notification/:id", function(req, res) {
		log.info(req.db, "PUT /notification/:id");
		triggerer.updateNotification(req.db, req.params.id, req.body.medium, req.body.text, req.body.detail1, req.body.detail2, req.body.detail3, function(error, result) {
			util.sendBack(res, error, result);	
		});
	});
	app.delete("/notification/:id", function(req, res) {
		log.info(req.db, "DELETE /notification/:id");
		triggerer.deleteNotification(req.db, req.params.id, function(error, result) {
			util.sendBack(res, error);	
		});
	});

	
	return app;
};
