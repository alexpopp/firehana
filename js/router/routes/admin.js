/*eslint no-console: 0, no-unused-vars: 0, no-shadow: 0, new-cap: 0*/
"use strict";
var express = require("express");
var scheduler = require("../../components/scheduler");
var configurator = require("../../components/configurator");
var util = require("../../utils/util");
var log = require("../../utils/log");

var connector = require("../../components/connector");
var dbaccess = require("../../utils/dbaccess");


module.exports = function() {
	var app = express.Router();

	app.get("/", function(req, res) {
		
	});
	
	app.get("/test", function(req, res) {
		console.log("test");
	});
	
	app.post("/config/test", function(req, res) {
		log.info(req.db, "POST /config/test");
		connector.test(req.db, req.body.medium, req.body.configuration, req.body.text, req.body.detail1, req.body.detail2, function(error) {	
			console.log("error: " + error);
			util.sendBack(res, error, "");  	
		});
	});
	
	app.get("/ping", function(req, res) {
		util.sendBack(res, undefined, "");  
	});
	
	app.post("/logs", function(req, res) {
		log.debug("POST /logs");
		var page = req.body.page ? req.body.page : 1;
		var messageFilter = req.body.messageFilter ? req.body.messageFilter : "";
		var levelFilter = req.body.levelFilter ? req.body.levelFilter : "";
		var userFilter = req.body.userFilter ? req.body.userFilter : "";
		var visibleLogEntries = 10;
		
		log.getLogs(req.db, page, visibleLogEntries, messageFilter, levelFilter, userFilter, function(error, logs, totalCount) {
			util.sendBack(res, error, {logs: logs, currentPage: page, maxPages: Math.ceil(totalCount / visibleLogEntries)});  
		});
	});
	
	app.get("/eventscheduler", function(req, res) {
		log.debug("GET /eventscheduler");
		scheduler.getActiveEventScheduler(req.db, function(error, result) {
			util.sendBack(res, error, result);
		});     
	});
	
	app.get("/notificationscheduler", function(req, res) {
		log.debug("GET /notificationscheduler");
		scheduler.getActiveNotificationScheduler(req.db, function(error, result) {
			util.sendBack(res, error, result);
		});      
	});
	
	//create or update event scheduler
	app.post("/eventscheduler", function(req, res) {
		log.info(req.db, "POST /eventscheduler_" + req.body.action);
		var action = req.body.action;
		var url = req.body.url;
		scheduler.getActiveEventScheduler(req.db, function(error, result) {
				if (error) {
					log.error(req.db, "admin.postEventscheduler getActiveEventScheduler", error);
					util.sendBack(res, error);
				} else if (action === "start") {
					var cronKey = req.body.cronKey;
					if (result) {
						scheduler.stopEventScheduler(req.db, function(error) {
							if (error) {
								log.error(req.db, "admin.postEventscheduler stopEventScheduler1", error);
								util.sendBack(res, error);
							} else {
								scheduler.startEventScheduler(req.db, cronKey, url, function(error) {
									if (error) {
										log.error(req.db, "admin.postEventscheduler startEventScheduler1", error);
									}
									util.sendBack(res, error);
								});	
							}
						});
					} else {
						scheduler.startEventScheduler(req.db, cronKey, url, function(error) {
							if (error) {
								log.error(req.db, "admin.postEventscheduler startEventScheduler2", error);
							}
							util.sendBack(res, error);
						});	
					}	
				} else if (action === "stop") {
					if (result) {
						scheduler.stopEventScheduler(req.db,  function(error) {
							if (error) {
								log.error(req.db, "admin.postEventscheduler stopEventScheduler2", error);
							}
							util.sendBack(res, error);
						});
					} else {
						util.sendBack(res, undefined);
					}					
				}
			}); 	
	});
	
	//create or update notification scheduler
	app.post("/notificationscheduler", function(req, res) {
		log.info(req.db, "POST /notificationscheduler_" + req.body.action);
		var action = req.body.action;
		var url = req.body.url;
		scheduler.getActiveNotificationScheduler(req.db, function(error, result) {
				if (error) {
					log.error(req.db, "admin.postNotificationscheduler getActiveNotificationScheduler", error);
					util.sendBack(res, error);
				} else if (action === "start") {
					if (result) {
						scheduler.stopNotificationScheduler(req.db, function(error) {
							if (error) {
								log.error(req.db, "admin.postNotificationscheduler stopNotificationScheduler1", error);
								util.sendBack(res, error);
							} else {
								scheduler.startNotificationScheduler(req.db, url, function(error) {
									if (error) {
										log.error(req.db, "admin.postNotificationscheduler startNotificationScheduler1", error);
									}
									util.sendBack(res, error);
								});	
							}
						});
					} else {
						scheduler.startNotificationScheduler(req.db, url, function(error) {
							if (error) {
								log.error(req.db, "admin.postNotificationscheduler startNotificationScheduler2", error);
							}
							util.sendBack(res, error);
						});	
					}	
				} else if (action === "stop") {
					if (result) {
						scheduler.stopNotificationScheduler(req.db,  function(error) {
							if (error) {
								log.error(req.db, "admin.postNotificationscheduler stopNotificationScheduler2", error);
							}
							util.sendBack(res, error);
						});
					} else {
						util.sendBack(res, undefined);
					}					
				}
			}); 	
	});
	
	app.get("/config/proxy", function(req, res) {
		log.debug( "GET /config/proxy");
		configurator.getProxyConfig(req.db, function(error, result) {
			util.sendBack(res, error, result);	
		});     
	});
	
	app.post("/config/proxy", function(req, res) {
		log.info(req.db, "POST /config/proxy");
		configurator.updateProxyConfig(req.db, req.body, function(error, result) {
			util.sendBack(res, error, result);	
		});
	});
	
	
	app.get("/config/mail", function(req, res) {
		log.debug( "GET /config/mail");
		configurator.getMailConfig(req.db, function(error, result) {
			util.sendBack(res, error, result);	
		});     
	});
	
	app.post("/config/mail", function(req, res) {
		log.info(req.db, "POST /config/mail");
		configurator.updateMailConfig(req.db, req.body, function(error, result) {
			util.sendBack(res, error, result);	
		});
	});
	
	app.get("/config/slack", function(req, res) {
		log.debug("GET /config/slack");
		configurator.getSlackConfig(req.db, function(error, result) {
			util.sendBack(res, error, result);	
		});     
	});
	
	app.post("/config/slack", function(req, res) {
		log.info(req.db, "POST /config/slack");
		configurator.updateSlackConfig(req.db, req.body, function(error, result) {
			util.sendBack(res, error, result);	
		});
	});
	
	app.get("/config/stride", function(req, res) {
		log.debug("GET /config/stride");
		configurator.getStrideConfig(req.db, function(error, result) {
			util.sendBack(res, error, result);	
		});     
	});
	
	app.post("/config/stride", function(req, res) {
		log.info(req.db, "POST /config/stride");
		configurator.updateStrideConfig(req.db, req.body, function(error, result) {
			util.sendBack(res, error, result);	
		});
	});
	
	app.get("/config/twilio", function(req, res) {
		log.debug("GET /config/twilio");
		configurator.getTwilioConfig(req.db, function(error, result) {
			util.sendBack(res, error, result);	
		});     
	});
	
	app.post("/config/twilio", function(req, res) {
		log.info(req.db, "POST /config/twilio");
		configurator.updateTwilioConfig(req.db, req.body, function(error, result) {
			util.sendBack(res, error, result);	
		});
	});
	
	app.get("/config/audit", function(req, res) {
		log.debug("GET /config/audit");
		configurator.getAuditConfig(req.db, function(error, result) {
			util.sendBack(res, error, result);	
		});     
	});
	
	app.post("/config/audit", function(req, res) {
		log.info(req.db, "POST /config/audit");
		configurator.updateAuditConfig(req.db, req.body, function(error, result) {
			util.sendBack(res, error, result);	
		});
	});
	
	app.get("/config/android", function(req, res) {
		log.debug("GET /config/android");
		configurator.getAndroidConfig(req.db, function(error, result) {
			util.sendBack(res, error, result);	
		});     
	});
	
	app.post("/config/android", function(req, res) {
		log.info(req.db, "POST /config/android");
		configurator.updateAndroidConfig(req.db, req.body, function(error, result) {
			util.sendBack(res, error, result);	
		});
	});
	
	app.get("/config/rest", function(req, res) {
		log.debug("GET /config/rest");
		configurator.getRESTConfig(req.db, function(error, result) {
			util.sendBack(res, error, result);	
		});     
	});
	
	app.post("/config/rest", function(req, res) {
		log.info(req.db, "POST /config/rest");
		configurator.updateRESTConfig(req.db, req.body, function(error, result) {
			util.sendBack(res, error, result);	
		});
	});
	
	app.get("/config/hangout", function(req, res) {
		log.debug("GET /config/hangout");
		configurator.getHangoutConfig(req.db, function(error, result) {
			util.sendBack(res, error, result);	
		});     
	});
	
	app.post("/config/hangout", function(req, res) {
		log.info(req.db, "POST /config/hangout");
		configurator.updateHangoutConfig(req.db, req.body, function(error, result) {
			util.sendBack(res, error, result);	
		});
	});

	return app;
};
