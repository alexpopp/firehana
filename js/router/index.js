"use strict";

module.exports = function(app, server){
	app.use("/scheduler", require("./routes/callback")());
	app.use("/api/admin", require("./routes/admin")());
	app.use("/api/trigger", require("./routes/trigger")());
};