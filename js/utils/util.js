/*eslint no-console: 0, no-unused-vars: 0*/
"use strict";
module.exports = {
	sendBack: function(res, error, message) {
		if (error) {
			if (error.message) {
				res.status(400).send(error.message);
			} else {
				res.status(400).send(error);
			}
		} else {
			if (!message) {
				message = {};
			}
			res.status(200).send(message);	
		}
	}
};
