var nodemailer = require("nodemailer");
var IncomingWebhook = require("@slack/client").IncomingWebhook;
var request = require("request");

module.exports = {

	sendMail: function(text, subject, to, host, port, secure, rejectUnauthorized, from, user, password, callback) {
		if (!text || !to || !subject || !host || !port || !from || !user || !password) {
			callback("Missing arguments");
			return;
		}
		
		// create reusable transporter object using the default SMTP transport
	    var transporter = nodemailer.createTransport({
	        host: host,
	        port: port,
	        secure: secure, // true for 465, false for other ports
	        auth: {
	            user: user, 
	            pass: password
	        }
	    });
	
	    // setup email data with unicode symbols
	    var mailOptions = {
	        from: from, // sender address
	        to: to, // list of receivers
	        subject: subject, // Subject line
	        text: text
	    };
	
	    // send mail with defined transport object
	    transporter.sendMail(mailOptions, function(error, info) {
	        callback(error, info);
	    });
	},
	sendSlack: function(text, channel, webhookUrl, username, callback) {
		if (!text || !channel || !webhookUrl || !username) {
			callback("Missing arguments");
			return;
		}
		
		var webhook = new IncomingWebhook(webhookUrl);
		webhook.send({"text": text, "channel": channel, "username": username, "icon_emoji": ":fire:"}, function(error) {
		    callback(error);
		});
	},
	sendStride: function(text, conversationUrl, accessToken, callback) {
		if (!text || !conversationUrl || !accessToken) {
			callback("Missing arguments");
			return;
		}
		
		request({
		    url: conversationUrl,
		    method: "POST",
		    json: true,
		    headers: {
			    "authorization": "Bearer " + accessToken,
			    "content-type": "application/json"	
		    },
		    body: {"version": 1,"type": "doc","content": [{"type": "paragraph","content": [{"type": "text","text": text}]}]}
		}, function (error, response){
			callback(error, response);
		});
	},
	sendTwilio: function(text, accountSid, authToken, toNumber, callback) {
		if (!text || !accountSid || !authToken || !toNumber) {
			callback("Missing arguments");
			return;
		}
		// require the Twilio module and create a REST client
		var client = require("twilio")(accountSid, authToken);

		client.messages
		  .create({
		    to: toNumber,
		    from: "Firehana",
		    body: text
		  }).then(function(message) {
			callback(undefined, message.sid);
		  }).catch(function(err) {
	    	callback(err);
	      });
	},
	sendAndroidPush: function(topic, text, fb_project_id, fb_private_key_id, fb_private_key, fb_client_email, fb_client_id, fb_client_x509_cert_url, callback) {
		var admin = require("firebase-admin");
	
		var serviceAccount = {
		  "type": "service_account",
		  "project_id": fb_project_id,
		  "private_key_id": fb_private_key_id,
		  "private_key": fb_private_key,
		  "client_email": fb_client_email,
		  "client_id": fb_client_id,
		  "auth_uri": "https://accounts.google.com/o/oauth2/auth",
		  "token_uri": "https://accounts.google.com/o/oauth2/token",
		  "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
		  "client_x509_cert_url": fb_client_x509_cert_url
		};
	
		admin.initializeApp({
		  credential: admin.credential.cert(serviceAccount),
		  databaseURL: "https://firehana-de92a.firebaseio.com"
		});
	
		var message = {
		   notification: {
		    title: "FIREHANA",
		    body: text
		  },
		  topic: topic
		};
	
		// Send a message to devices subscribed to the provided topic.
		admin.messaging().send(message)
		  .then(function(response) {
		    // Response is a message ID string.
		    callback(undefined, response);
		  })
		  .catch(function(error) {
		    callback(error);
		  });
	},
	sendREST: function(text, url, method, callback) {
		request({
		    url: url,
		    method: method,
		    json: true,
		    headers: {
			    "content-type": "application/json"	
		    },
		    body: {"text": text}
		}, function (error, response){
			callback(error, response);
		});
	},
	sendHangoutChat: function(text, webhookUrl, callback) {
		request({
		    url: webhookUrl,
		    method: "POST",
		    json: true,
		    headers: {
			    "content-type": "application/json"	
		    },
		    body: {"text" : text}
		}, function (error, response){
		    callback(error, response);
		});
	}
};