/*eslint no-console: 0, no-unused-vars: 0*/
"use strict";
var C = require("./constants");
var hdbext = require("@sap/hdbext");

module.exports = {
	execute: function(db, query, resultCallback) {
		var async = require("async");

		async.waterfall([
			function prepare(callback) {
				db.prepare(query,
					function(err, statement) {
						callback(null, err, statement);
					});
			},

			function execute(err, statement, callback) {
				if (err) {
					resultCallback(err, undefined);	
				} else {
					statement.exec([], function(execErr, results) {
						callback(null, execErr, results);
					});	
				}
			},
			function response(err, results, callback) {
				if (err) {
					resultCallback(err.toString(), undefined);
					return;
				} else {
					resultCallback(undefined, results);
				}
				callback();
			}
		]);
	},
	
	
	secureStore_insertOrUpdate: function(db, key, value, callback) {
		var self = this;
		this.secureStore_retrieve(db, key, function(error, entry) {
			if (error) {
				callback(error);	
			} else {
				if (entry) {
					db.prepare("CALL \"SYS\".\"USER_SECURESTORE_DELETE\"(?, ?, ?);", function(err, statement) {
						if (err) {
							console.log("dbaccess.secureStore_insertOrUpdate prepare_USER_SECURESTORE_DELETE: " + err);
							callback(err);
						} else {
							statement.exec([C.SECURESTORE_STORENAME, true, key], function(execErr, results) {
									if (execErr) {
										console.log("dbaccess.secureStore_insertOrUpdate exec_USER_SECURESTORE_DELETE: " + execErr);
										callback(execErr);	
									} else {
										self.secureStore_insert(db, key, value, callback);
									}
								});	
						}
					});	
				} else {
					self.secureStore_insert(db, key, value, callback);	
				}
			}	
		});	
	},
	secureStore_retrieve: function(db, key, callback) {
		db.prepare("CALL \"SYS\".\"USER_SECURESTORE_RETRIEVE\"(?, ?, ?, ?);", function(err, statement) {
			if (err) {
				console.log("dbaccess.secureStore_retrieve prepare_USER_SECURESTORE_RETRIEVE: " + err);
				callback(err);
			} else {
				statement.exec([C.SECURESTORE_STORENAME, true, key], function(execErr, results) {
					if (execErr) {
						console.log("dbaccess.secureStore_retrieve exec_USER_SECURESTORE_RETRIEVE: " + execErr);
						callback(error);
					} else if (results && results.VALUE) {
						var result = Buffer(results.VALUE).toString("utf8");
						callback(undefined, result);
					} else {
						callback(undefined, undefined);
					}
				});	
			}
		});	
	},
	secureStore_insert: function(db, key, value, callback) {
		db.prepare("CALL \"SYS\".\"USER_SECURESTORE_INSERT\"(?, ?, ?, ? );", function(err, statement) {
			if (err) {
				console.log("dbaccess.secureStore_insert USER_SECURESTORE_INSERT: " + err);
				callback(err);
			} else {
				statement.exec([C.SECURESTORE_STORENAME, true, key, Buffer.from(value, "utf8")], function(execErr, results) {
						callback(execErr);
					});	
			}
		});	
	},
	getCurrentUser: function(db, callback) {
		this.execute(db, "SELECT SESSION_CONTEXT('APPLICATIONUSER') \"APPLICATION_USER\" FROM \"DUMMY\"", function(error, result) { 
			if (!error && result[0] && result[0].APPLICATION_USER) {
				callback(undefined, result[0].APPLICATION_USER);
			} else {
				callback(error, undefined);
			}
		});	
	},
	getConfig: function(db, key, callback) {
		this.execute(db, "select * from \"" + C.TABLE_CONFIG + "\" where \"mkey\" = '" + key + "'", function(error, result) {
			if (error) {
				callback(error, undefined);
			} else {
				if (result[0] !== undefined) {
					if (result[0].mvalue1 === "true") {
	    				result[0].mvalue1 = true;
					}
					if (result[0].mvalue2 === "true") {
	    				result[0].mvalue2 = true;
					}
					
					if (result[0].mvalue1 === "false") {
	    				result[0].mvalue1 = false;
					}
					if (result[0].mvalue2 === "false") {
	    				result[0].mvalue2 = false;
					}
	    		} 
	    		
	    		if (result[0]) {
	    			callback(error, result[0].mvalue1, result[0].mvalue2);	
	    		} else {
	    			callback(error, undefined);	
	    		}
			}
		});
	},
	saveOrUpdateConfig: function(db, key, value1, value2, callback) {
		var self = this;
		this.getConfig(db, key, function(error, result) {
			if (error) {
				console.log("dbaccess.saveOrUpdateConfig getConfig: " +  error);
				callback(error);
			} else if (result !== undefined) {
				self.execute(db, "update \"" + C.TABLE_CONFIG + "\" set \"mvalue1\" = '" + value1 + "', \"mvalue2\" = '" + value2 + "' where \"mkey\" = '" + key + "'", callback);	
			} else {
				self.execute(db, "insert into \"" + C.TABLE_CONFIG + "\" values('"+ key + "', '" + value1 + "', '" + value2 + "')", callback);	
			}
		});
	},
	deleteConfig: function(db, key, callback) {
		this.execute(db, "delete from \"" + C.TABLE_CONFIG + "\" where \"mkey\" = '" + key + "'", function(error) {
			callback(error);	
		});
	}
};
