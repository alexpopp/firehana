/*eslint no-console: 0, no-unused-vars: 0*/
"use strict";
var xsenv = require("@sap/xsenv");
var dbaccess = require("./dbaccess");
var C = require("./constants");
var clone = require("clone");

function logToConsole(level, msg) {
	console.log(level + ": " + msg);
}

function logToDatabase(db, level, msg) {
	xsenv.loadEnv();
	var credentials = xsenv.getServices({
		auditlog: "firehana-audit-log"
	}).auditlog;
	dbaccess.getCurrentUser(db, function(error, user) {
		var applicationuser = credentials.user;
		if (user) {
			applicationuser = user;
		} 
		dbaccess.execute(db, "insert into \"" + C.TABLE_LOGENTRY + "\" values('"+ msg + "', CURRENT_TIMESTAMP, '" + applicationuser + "', '" + level + "')", function(error2, result2) {
			if (error2) {
				logToConsole("ERROR", error2);	
			}
		});	
	});
}

function logToAudit(db, level, msg) {
	/*
	WORKED IN SPS2 (@sap/audit-logging": "2.1.1",)
	xsenv.loadEnv();
	var credentials = xsenv.getServices({
		auditlog: "firehana-audit-log"
	}).auditlog;
	
	var auditLog = require("@sap/audit-logging")(credentials);
	dbaccess.getCurrentUser(db, function(error, user) {
			var applicationuser = credentials.user;
			if (user) {
				applicationuser = user;
			} 
			console.log("audit log message with user: " + applicationuser);
			auditLog.securityMessage(msg)
				.by(applicationuser) 
				.log(function(err) {
					if (err) {
						logToConsole("ERROR", err);	
						logToDatabase("ERROR", err);
					}
				  });
	});*/
	
	xsenv.loadEnv();
	var credentials = xsenv.getServices({
		auditlog: "firehana-audit-log"
	}).auditlog;
	var auditLogging = require('@sap/audit-logging');
	auditLogging.v2(credentials, function(err, auditLog) {
		if (err) {
			// if the Audit log server does not support version 2 of the REST APIs
			// an error in the callback is returned
			logToConsole("ERROR", err);	
			logToDatabase("ERROR", err); //TODO: maybe switch to v1?
		} else {
			auditLog.securityMessage(msg).by("USER TODOD").log(function(err2) {
					if (err2) {
						logToConsole("ERROR", err2);	
						logToDatabase("ERROR", err2);
						return;
					}
				});
		}
		return null;
	});
}

module.exports = {
	debug: function(msg) {
		logToConsole("DEBUG", msg);
	},
	info: function(db, msg) {
		var copyOfDb = clone(db); 
		logToConsole("INFO", msg);
		logToDatabase(copyOfDb, "INFO", msg);	
	},
	error: function(db, context, msg) {
		var copyOfDb = clone(db); 
		if (msg.message) {
			msg = msg.message;
		}
		msg = JSON.stringify(msg);
		
		logToConsole("ERROR", "(" + context + ") " + msg);	
		logToDatabase(copyOfDb, "ERROR", "(" + context + ") " + msg);	
	},
	audit: function(db, msg) {
		var copyOfDb = clone(db); 
		logToAudit(copyOfDb, "AUDIT", msg);
	},
	
	//horribly inefficient, especially on a column store, but it shouldnt be used very often...
	getLogs: function(db, page, visibleLogEntries,  messageFilter, levelFilter, userFilter, callback) {
		var self = this;
		var offset = (page-1) * visibleLogEntries;
		var whereClause = "where \"mmessage\" LIKE '%" + messageFilter + "%' and \"mlevel\" LIKE '%" + levelFilter + "%' and \"muser\" LIKE '%" + userFilter + "%'";
			
		dbaccess.execute(db, "select * from \"" + C.TABLE_LOGENTRY + "\" " + whereClause + " order by \"mtimestamp\" desc limit " + visibleLogEntries + " offset " + offset, function(error, result) {
				
			if (error) {
				self.error(db, "log.getLogs execute1", error);
				callback(error, [], 0);
				return;
			}
			dbaccess.execute(db, "select count('*') as COUNT from \"" + C.TABLE_LOGENTRY + "\" " + whereClause, function(error2, result2) {
				if (error2) {
					self.error(db, "log.getLogs execute2", error2);
				}
				if (result2[0]) {
					callback(error2, result, result2[0].COUNT);	
				} else {
					callback(error2, result, 0);
				}
			});	
		});	
	}
};
